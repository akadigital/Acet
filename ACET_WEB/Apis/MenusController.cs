﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ACET_WEB.Models;
using System.Net;
using ExtensionMethods;
namespace ACET_WEB.Controllers
{
	[Authorize(Roles = "Administrator")]
    public class MenusController : ApiController
    {
        public WrapUserSerial GetAllMenus([FromUri] int draw, [FromUri] int length, [FromUri] int start,
          [FromUri] string search_value, [FromUri] string order_colunm, [FromUri] string order_dir)
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var rs = new WrapUserSerial();
                var rs_data = new List<List<dynamic>>();
                var users = dc.Menus.Where(c => c.Is_Active==1 && c.ParentId==null);
                if (search_value != null)
                {
                    if (!search_value.Trim().Equals(""))
                    {
                        users = users.Where(c => c.Name.Contains(search_value) || c.NameEng.Contains(search_value));
                    }
                }
                switch (order_colunm)
                {
                    case "0":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }
                        break;
                    case "1":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Name);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Name);
                        }
                        break;
                  
                    case "2":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Modified);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Modified);
                        }
                        break;
                    default:
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }

                        break;
                }
				var filter = users;
				filter = filter.Skip(start).Take(length);
				foreach (var user in filter.ToArray())
                    {
                        var item = new List<dynamic>();
                        item.Add(user.Id.ToString());
                        item.Add(user.Name);
                        item.Add(user.NameEng);
                        item.Add(user.Modified.ToString());
                        var childItems = new List<List<dynamic>>();
                       
                        foreach (var child in user.Menus1)
                        {
                            var childItem = new List<dynamic>();
                            childItem.Add(child.Id.ToString());
                            childItem.Add(child.Name.ToString());
                            childItem.Add(child.NameEng.ToString());
                            childItem.Add(child.Modified.ToString());
                            childItems.Add(childItem);
                        }
                        item.Add(childItems);
                        rs_data.Add(item);
                    }
                
                
                rs.draw = draw;
				rs.recordsFiltered = users.Count();
				rs.recordsTotal = users.Count();
                rs.data = rs_data;
                return rs;
            }
        }
         public MenuTree Get(int id)
         {
             using (acet_dbEntities dc = new acet_dbEntities())
             {
                 var menu = dc.Menus.FirstOrDefault(c => c.Id.Equals(id));
                 if (menu == null)
                 {
                     throw new HttpResponseException(HttpStatusCode.NotFound);
                 }
                 else {
					MenuTree menutree = new MenuTree ();
					menutree.ID = menu.Id;
					menutree.Name = menu.Name;
					menutree.NameEng = menu.NameEng;
					menutree.Link = menu.Value;
					menutree.LinkEng = menu.Status;
					menutree.Order = (int)menu.Order;
					if(menu.ParentId!=null)
						menutree.ParentId = (int) menu.ParentId;

					return menutree;
                 }
                 
             }

           
         }
		public Boolean Post(int id) {
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{   
					var u = dc.Menus.FirstOrDefault(c => c.Id.Equals(id));
					if(u.Menus1.Count > 0 ){
						foreach(Menu cu in u.Menus1.ToList()){
							dc.Menus.DeleteObject (cu);
						}
					}

					dc.Menus.DeleteObject(u);
					dc.SaveChanges();
					return true;
				}
				catch (Exception ex)
				{
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
					throw ex;
				}
			}
		}
		public Boolean Post([FromBody] MenuTree menuform)
		{
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{
					if (menuform.ID>0)
					{
						var u = dc.Menus.FirstOrDefault(c => c.Id.Equals(menuform.ID));

						u.Modified = DateTime.Now;
						u.Name = menuform.Name;
						u.NameEng = menuform.NameEng;
						u.Order = menuform.Order;
						if(menuform.ParentId > 0){
							u.ParentId = menuform.ParentId;
						}else{
							u.ParentId = null;
						}
						u.Slug = u.Name.NonUnicode();
						u.SlugEng = u.NameEng.NonUnicode();
						u.Value = menuform.Link;
						u.Type = menuform.Type;
						u.Status = menuform.LinkEng;
						dc.SaveChanges();
						return true;
					}
					else
					{
						Menu u = new Menu();
						u.Created = DateTime.Now;
						u.Modified = DateTime.Now;
						u.Name = menuform.Name;
						u.NameEng = menuform.NameEng;
						u.Order = menuform.Order;
						if(menuform.ParentId > 0){
							u.ParentId = menuform.ParentId;
						}else{
							u.ParentId = null;
						}
						u.Slug = u.Name.NonUnicode();
						u.SlugEng = u.NameEng.NonUnicode();
						u.Value = menuform.Link;
						u.Status = menuform.LinkEng;
						u.Type = menuform.Type;

						dc.Menus.AddObject(u);
						dc.SaveChanges();
						return true;
					}  
				}
				catch (Exception ex)
				{
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
					throw ex;
				}   
			}
		}
         
    }
}
