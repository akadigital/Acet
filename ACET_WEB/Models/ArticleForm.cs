﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Models
{
    
    public class ArticleForm
    {
        public int ID { get; set; }
        public string title_vi { get; set; }
        public string title_en { get; set; }
        public string desc_vi { get; set; }
        public string desc_en { get; set; }
        public string img { get; set; }
        public string banner { get; set; }
        public string banner_m { get; set; }
        public string cusPoint{ get; set; }
        public int order { get; set; }
		public int status { get; set; }
		public int location { get; set; }
        [AllowHtml]
        public string content_vi { get; set; }
        [AllowHtml]
        public string content_en { get; set; }
		public string keywords { get; set; }
		public string metadesc { get; set; }
		public string is_draft { get; set; }
		public DateTime created { get; set;}
    }
}