﻿using ACET_WEB.App_Start;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LocalizationAttribute("vi"), 0);
        }
    }
}