﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACET_WEB.Models;
using ExtensionMethods;
using System.IO;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Data.Entity;
using ACET_WEB.App_Start;

namespace ACET_WEB.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

		public ActionResult Review(int id, string post_type=""){
			using (acet_dbEntities db = new acet_dbEntities ()) {
				var view = "index";
				return View (view);
			}
		}
        public ActionResult Index()
        {
            return View();
        }
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Account(User user)
		{
			using (acet_dbEntities db = new acet_dbEntities())
			{
				if (ModelState.IsValid)
				{
					
					db.Users.Attach(user);
					user.Password = Helper.EncodeSHA1(user.Password);
					user.modified = DateTime.Now;
					user.created = DateTime.Now;
					user.Is_Active = 1;
					db.ObjectStateManager.ChangeObjectState(user, EntityState.Modified);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				return View(user);
			}
		}
		public ActionResult Account()
		{
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				if (Session ["UserID"] != null) {
					int c_id = (int)Session ["UserID"];
					var model = dc.Users.FirstOrDefault (u => u.Id == c_id);
					return View(model);
				} else {
					return Redirect("/login/logout/");
				}

			}
		}
        #region Articles
        [HttpGet]
        public ActionResult Articles(string method="list", string type="post") {
			if (Request.QueryString ["type"] == null) {
				return Redirect("/admin");
			}
			ArticleForm article = new ArticleForm();
            switch (method)
            {
                case "list":
                    return View("ListArticles");
				case "addnew":
					ViewData ["post"] = article;
					article.created = DateTime.Now;
                    return View("Articles");
                case "edit":
                     int id = 0;
                    try
                    {
                        id = Int32.Parse(Request.QueryString["id"]);
                    }
                    catch (Exception ex)
                    {

                    }
                    if (id > 0)
                    {
                        using (acet_dbEntities dc = new acet_dbEntities())
                        {
                            var p = dc.Posts.FirstOrDefault(s => s.Id==id);
                            if (p==null)
                            {
                                return View("ListArticles");
                            }
                            else
                            {
                                
                                article.ID = p.Id;
                                article.title_vi = p.Title;
                                article.title_en = p.TitleEng;
                                article.desc_vi = p.Desc;
                                article.desc_en = p.DescEng;
                                article.content_vi = p.Content;
							    article.created = (DateTime)p.Created;
                                article.content_en = p.ContentEng;
                                article.img = p.Image;
								article.status =  (int)p.Status;
                                article.order = (int)p.order;
								article.cusPoint = p.CusPoint.ToString();
								if (p.LocationId != null)
									article.location = (int)p.LocationId;
								else
									article.location = 1;
								if (article.location == 0) {
									article.location = 1;
								}
								using (SimpleContext ct = new SimpleContext ()) {
									var postmeta_kw = ct.PostMetas.FirstOrDefault (pm => pm.PostId == p.Id && pm.Name.Equals ("keywords"));
									if (postmeta_kw != null) {
										article.keywords = postmeta_kw.Value;
									}
									var postmeta_desc = ct.PostMetas.FirstOrDefault (pm => pm.PostId == p.Id && pm.Name.Equals ("metadesc"));
									if (postmeta_desc != null) {
										article.metadesc = postmeta_desc.Value;
									}
                                    var postmeta_banner = ct.PostMetas.FirstOrDefault(pm => pm.PostId == p.Id && pm.Name.Equals("banner"));
                                    if (postmeta_banner != null)
                                    {
                                        article.banner = postmeta_banner.Value;
                                    }
                                    var postmeta_banner_m = ct.PostMetas.FirstOrDefault(pm => pm.PostId == p.Id && pm.Name.Equals("banner_m"));
                                    if (postmeta_banner_m != null)
                                    {
                                        article.banner_m = postmeta_banner_m.Value;
                                    }

                                }
                                ViewData["post"] = article;
                            }
                        }
                    }
                    return View("Articles");
                default:
                    return View("ListArticles");
                   
            }
           
        }


		private void addOrUpdateMeta(int postID, string name,string value, string lang ="vi"){
			using(SimpleContext ct = new SimpleContext ()){

				var post = ct.PostMetas.FirstOrDefault (p => p.Name.Equals (name) && p.PostId == postID);
				if (post == null) {
					post = new PostMeta {
						PostId=postID,
						Name=name,
						Value=value,
						lang=lang
					};
					ct.PostMetas.Add (post);
				} else {
					post.PostId = postID;
					post.Name = name;
					post.Value = value;
					post.lang = lang;
				}
				ct.SaveChanges ();
			
			}
		}

        [HttpPost]
        public ActionResult Articles(ArticleForm article, string method = "list", string type = "post")
        {
			var p_status = 0;
			var is_draft = -1;
			if (article.is_draft != null && !article.is_draft.Equals ("")) {
				p_status = -1;
				is_draft = Int32.Parse(article.is_draft);
				if (is_draft == 0) {
					p_status = 0;
				} 
				if (is_draft == -1) {
					p_status = 1;
				}
			} else {
					p_status = 1;
			}
            if (method.Equals("addnew"))
            {
                using (acet_dbEntities dc = new acet_dbEntities())
                {
                    Post p = new Post();
					switch (type)
                    {
                        case "su-kien":
                            p.CategoryId = 73;
                            break;
                        case "tin-uu-dai":
                            p.CategoryId = 72;
                            break;
                        case "tin-doi-tac":
                            p.CategoryId = 74;
                            break;
                        case "tin-bao-chi":
                            p.CategoryId = 90;
                            break;
                        case "testimonial":
                            p.CategoryId = 76;
                            break;
                        case "testimonial-fs":
                            p.CategoryId = 92;
                            break;
                        case "recruitment":
                            p.CategoryId = 78;
                            break;
                        case "recruitment-academic":
                            p.CategoryId = 77;
                            break;
                        case "hb-acet":
                            p.CategoryId = 94;
                            break;
                        case "idp":
                            p.CategoryId = 95;
                            break;
                        case "first-step":
                            p.CategoryId = 96;
                            break;
                        case "academic-english":
                            p.CategoryId = 97;
                            break;
                        case "ielts":
                            p.CategoryId = 98;
                            break;
                        case "gioi-thieu":
                            p.CategoryId = 45;
                            break;
                        case "page":
                            p.CategoryId = 99;
                            break;
						case "teacher":
							p.CategoryId = 100;
							break;
						case "corp-hn":
							p.CategoryId = 101;
							break;
						case "corp-hcm":
							p.CategoryId = 102;
							break;
						case "tips":
							p.CategoryId = 103;
							break;
						case "full-page":
							p.CategoryId = 104;
							break;
						default:
							break;
                    }

                    p.Title = article.title_vi;
                    p.TitleEng = article.title_en;
                    p.Slug = p.Title.NonUnicode();
					if (p.TitleEng!=null)
                    	p.SlugEng = p.TitleEng.NonUnicode();
                    p.Desc = article.desc_vi;
                    p.DescEng = article.desc_en;
                    p.Content = article.content_vi;
                    p.ContentEng = article.content_en;
                    p.Image = article.img;
                    p.order = article.order;
					if (article.created == null) {
						p.Created = DateTime.Now;
					} else {
						p.Created = article.created;
					}
                    
                    p.Modified = DateTime.Now;
                    p.Type = type;
					p.LocationId = article.location;
					if (Session ["UserID"] == null || (int)Session ["UserID"] == 0) {
						Redirect ("/login/logout/");
					} else {
						p.UserId = (int)Session["UserID"];
					}
                    
					if (p.Content!=null && p.Title!=null)
                    	p.ContentIndex = p.Title.MakeIndexData() + p.Content.MakeIndexData();
					if(p.ContentEng!=null &&p.TitleEng!=null)
                	    p.ContentIndexEnd = p.TitleEng.MakeIndexData() + p.ContentEng.MakeIndexData();
					p.Status = p_status;
               	 	
					if (type.Equals ("corp-hn") || type.Equals ("corp-hcm")) {
						p.Status = 0;
					}
					if (type.Equals ("testimonial") || type.Equals ("testimonial-fs")) {
						p.CusPoint = Decimal.Parse(article.cusPoint);
					}
                    dc.Posts.AddObject(p);

                    dc.SaveChanges();
					if (article.keywords != null && !article.keywords.Equals ("")) {
						addOrUpdateMeta (p.Id, "keywords", article.keywords );
					}
					if (article.metadesc != null && !article.metadesc.Equals ("")) {
						addOrUpdateMeta (p.Id, "metadesc", article.metadesc);
					}
                    if (article.banner != null && !article.banner.Equals(""))
                    {
                        addOrUpdateMeta(p.Id, "banner", article.banner);
                    }
                    if (article.banner_m != null && !article.banner_m.Equals(""))
                    {
                        addOrUpdateMeta(p.Id, "banner_m", article.banner_m);
                    }
                    if (p_status == -1) {
						return Redirect ("/Review/?id=" + p.Id.ToString ());
					}
                }
               
            }
            if (method.Equals("edit") && Int32.Parse(Request.QueryString["id"]) > 0)
            {
                int id = 0;
                try
                {
                    id = Int32.Parse(Request.QueryString["id"]);
                }
                catch (Exception ex)
                {

                }
                using (acet_dbEntities dc = new acet_dbEntities())
                {
					

                    var p = dc.Posts.FirstOrDefault(s => s.Id == id);
                    if (p == null)
                    {
                        return Redirect(Request.Url.AbsoluteUri);
                    }
                    else
                    {
						var is_add_new = 0;
						if (is_draft > 0) {
							SimpleContext ct = new SimpleContext ();
							var draft = ct.PostMetas.FirstOrDefault (p1 => p1.PostId == id && p1.Name.Equals ("draft_id"));
							if (draft != null) {
								int draft_id = Int32.Parse (draft.Value);
								p = dc.Posts.FirstOrDefault (s => s.Id == draft_id);
								if (p == null) {
									p = new Post ();
									if (article.created == null) {
										p.Created = DateTime.Now;
									} else {
										p.Created = article.created;
									}

									is_add_new = 1;
									switch (type)
									{
									case "su-kien":
										p.CategoryId = 73;
										break;
									case "tin-uu-dai":
										p.CategoryId = 72;
										break;
									case "tin-doi-tac":
										p.CategoryId = 74;
										break;
									case "tin-bao-chi":
										p.CategoryId = 90;
										break;
									case "testimonial":
										p.CategoryId = 76;
										break;
									case "testimonial-fs":
										p.CategoryId = 92;
										break;
									case "recruitment":
										p.CategoryId = 78;
										break;
									case "recruitment-academic":
										p.CategoryId = 77;
										break;
									case "hb-acet":
										p.CategoryId = 94;
										break;
									case "idp":
										p.CategoryId = 95;
										break;
									case "first-step":
										p.CategoryId = 96;
										break;
									case "academic-english":
										p.CategoryId = 97;
										break;
									case "ielts":
										p.CategoryId = 98;
										break;
									case "gioi-thieu":
										p.CategoryId = 45;
										break;
									case "page":
										p.CategoryId = 99;
										break;
									case "teacher":
										p.CategoryId = 100;
										break;
									case "corp-hn":
										p.CategoryId = 101;
										break;
									case "corp-hcm":
										p.CategoryId = 102;
										break;
									case "tips":
										p.CategoryId = 103;
										break;
									case "full-page":
										p.CategoryId = 104;
										break;
									default:
										break;
									}
								}
							} else {
								p = new Post ();
								if (article.created == null) {
									p.Created = DateTime.Now;
								} else {
									p.Created = article.created;
								}

								is_add_new = 1;
								switch (type)
								{
								case "su-kien":
									p.CategoryId = 73;
									break;
								case "tin-uu-dai":
									p.CategoryId = 72;
									break;
								case "tin-doi-tac":
									p.CategoryId = 74;
									break;
								case "tin-bao-chi":
									p.CategoryId = 90;
									break;
								case "testimonial":
									p.CategoryId = 76;
									break;
								case "testimonial-fs":
									p.CategoryId = 92;
									break;
								case "recruitment":
									p.CategoryId = 78;
									break;
								case "recruitment-academic":
									p.CategoryId = 77;
									break;
								case "hb-acet":
									p.CategoryId = 94;
									break;
								case "idp":
									p.CategoryId = 95;
									break;
								case "first-step":
									p.CategoryId = 96;
									break;
								case "academic-english":
									p.CategoryId = 97;
									break;
								case "ielts":
									p.CategoryId = 98;
									break;
								case "gioi-thieu":
									p.CategoryId = 45;
									break;
								case "page":
									p.CategoryId = 99;
									break;
								case "teacher":
									p.CategoryId = 100;
									break;
								case "corp-hn":
									p.CategoryId = 101;
									break;
								case "corp-hcm":
									p.CategoryId = 102;
									break;
								case "tips":
									p.CategoryId = 103;
									break;
								case "full-page":
									p.CategoryId = 104;
									break;
								default:
									break;
								}
							}
						}
                        p.Title = article.title_vi;
                        p.TitleEng = article.title_en;
                        p.Slug = p.Title.NonUnicode();
						if(p.TitleEng != null) {
							p.SlugEng = p.TitleEng.NonUnicode();
						}
						if (article.created != null) {
							p.Created = article.created;
						}

                        p.Desc = article.desc_vi;
						p.Type = type;
                        p.DescEng = article.desc_en;
                        p.Content = article.content_vi;
                        p.ContentEng = article.content_en;
						p.LocationId = article.location;
						if (p.Content!=null && p.Title!=null)
							p.ContentIndex = p.Title.MakeIndexData() + p.Content.MakeIndexData();
						if(p.ContentEng!=null &&p.TitleEng!=null)
							p.ContentIndexEnd = p.TitleEng.MakeIndexData() + p.ContentEng.MakeIndexData();
                        
                        p.Modified = DateTime.Now;
                        p.Image = article.img;
                        p.order = article.order;
						p.Status = p_status;
						if (Session ["UserID"] == null || (int)Session ["UserID"] == 0) {
							Redirect ("/login/logout/");
						} else {
							p.UserId = (int)Session["UserID"];
						}
						if (type.Equals ("corp-hn") || type.Equals ("corp-hcm")) {
							p.Status = 0;
						}
						if (type.Equals ("testimonial") || type.Equals ("testimonial-fs")) {
							p.CusPoint = Decimal.Parse(article.cusPoint);
						}

						if (is_add_new > 0) {
							dc.Posts.AddObject (p);
						}
                        dc.SaveChanges();
						if (article.keywords != null && !article.keywords.Equals ("")) {
							addOrUpdateMeta (p.Id, "keywords", article.keywords );
						}
						if (article.metadesc != null && !article.metadesc.Equals ("")) {
							addOrUpdateMeta (p.Id, "metadesc", article.metadesc);
						}
                        if (article.banner != null && !article.banner.Equals(""))
                        {
                            addOrUpdateMeta(p.Id, "banner", article.banner);
                        }
                        if (article.banner_m != null && !article.banner_m.Equals(""))
                        {
                            addOrUpdateMeta(p.Id, "banner_m", article.banner_m);
                        }

                        if (is_draft > 0) {
							addOrUpdateMeta (id, "draft_id", p.Id.ToString());
						}
                    }
					if (p_status == -1) {
						return Redirect ("/Review/?id=" + p.Id.ToString ());
					}
                }
            }

            return Redirect("/admin/articles/?type="+type);
        }
        #endregion

        #region Categories
        [HttpGet]
        public ActionResult Categories(string method = "list")
        {
            switch (method)
            {
                case "list":
                    return View("ListCategories");
                case "addnew":
                    return View("Categories");
                case "edit":
                    int id = 0;
                    try
                    {
                        id = Int32.Parse(Request.QueryString["id"]);
                    }
                    catch (Exception ex)
                    {

                    }
                    if (id > 0)
                    {
                        using (acet_dbEntities dc = new acet_dbEntities())
                        {
                            var p = dc.Categories.FirstOrDefault(s => s.Id == id);
                            if (p == null)
                            {
                                return View("ListCategories");
                            }
                            else
                            {
                                CategoryForm article = new CategoryForm();
                                article.ID = p.Id;
                                article.name = p.Name;
                                article.name_en = p.NameEng;
                                article.img = p.Img;
                                article.img_mobile = p.ImgMobile;
								using (SimpleContext ct = new SimpleContext ()) {
									var postmeta_link = ct.PostMetas.FirstOrDefault (pm => pm.PostId == p.Id && pm.Name.Equals ("img_link"));
									if (postmeta_link != null) {
										article.img_link = postmeta_link.Value;
									}
									var postmeta_link_en = ct.PostMetas.FirstOrDefault (pm => pm.PostId == p.Id && pm.Name.Equals ("img_link_en"));
									if (postmeta_link_en != null) {
										article.img_link_en = postmeta_link_en.Value;
									}
								}
                                ViewData["post"] = article;
                            }
                        }
                    }
                    return View("Categories");
                default:
                    return View("ListCategories");

            }
        }
        [HttpPost]
        public ActionResult Categories(CategoryForm article, string method = "addnew")
        {
            if (method.Equals("addnew"))
            {
                using (acet_dbEntities dc = new acet_dbEntities())
                {
                    Category p = new Category();
                    p.Name = article.name;
                    p.NameEng = article.name_en;
                    p.Slug = p.Name.NonUnicode();
                    p.SlugEng = p.NameEng.NonUnicode();
                    p.Img = article.img;
                    p.ImgMobile = article.img_mobile;
                    p.Created = DateTime.Now;
                    p.Modified = DateTime.Now;
                    p.Status = 1;
                    dc.Categories.AddObject(p);
                    dc.SaveChanges();
					addOrUpdateMeta (p.Id, "img_link", article.img_link );
					addOrUpdateMeta (p.Id, "img_link_en", article.img_link_en );
                }

            }
            if (method.Equals("edit") && Int32.Parse(Request.QueryString["id"]) > 0)
            {
                int id = 0;
                try
                {
                    id = Int32.Parse(Request.QueryString["id"]);
                }
                catch (Exception ex)
                {

                }
                using (acet_dbEntities dc = new acet_dbEntities())
                {
                    var p = dc.Categories.FirstOrDefault(s => s.Id == id);
                    if (p == null)
                    {
                        return Redirect(Request.Url.AbsoluteUri);
                    }
                    else
                    {
                        p.Name = article.name;
                        p.NameEng = article.name_en;
                        p.Slug = p.Name.NonUnicode();
                        p.SlugEng = p.NameEng.NonUnicode();
                        p.Status = 1;
                        p.Img = article.img;
                        p.ImgMobile = article.img_mobile;
                        p.ImgLink = article.img_link;
                        p.Modified = DateTime.Now;
                        dc.SaveChanges();
						addOrUpdateMeta (p.Id, "img_link", article.img_link );
						addOrUpdateMeta (p.Id, "img_link_en", article.img_link_en );
                    }

                }
            }
            return Redirect("/admin/categories");
        }
        #endregion

        #region Users
        public ActionResult Users()
        {
            return View();
        }
        #endregion

        #region Menus
		public ActionResult Menus()
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
				string type=this.Request.QueryString["type"];
				if (type == null || type.Equals("")) {
					type = "top";
				}
				var menus = dc.Menus.Where(m => m.ParentId == null && m.Type.Equals(type)).ToArray();
                var view_rs = new List<MenuTree>();
                foreach (var item in menus)
                {
                    var view_item = new MenuTree();
                    view_item.ID = item.Id;
                    view_item.Name = item.Name;
                    view_item.NameEng = item.NameEng;
                    view_item.HasChild = false;
                    view_item.Childs = new List<MenuTree>();
                    view_item.Modified = (DateTime)item.Modified;
                    view_item.Order = (int)item.Order;
                    if (item.Menus1.Count > 0)
                    {
                        view_item.HasChild = true;
                        foreach (var childitem in item.Menus1.ToArray())
                        {
                            var view_childitem = new MenuTree();
                            view_childitem.ID = childitem.Id;
                            view_childitem.Name = childitem.Name;
                            view_childitem.NameEng = childitem.NameEng;
                            view_childitem.Modified = (DateTime)childitem.Modified;
                            view_childitem.HasChild = false;
                            view_childitem.Childs = new List<MenuTree>();
                            view_childitem.Order = (int)childitem.Order;
                            view_item.Childs.Add(view_childitem);     
                        }
                       
                    }
                    view_rs.Add(view_item);
                }
                ViewData["menus"] = view_rs;
				ViewBag.type = type;
            }
           
            return View(); 
        }

        #endregion

        #region Modules
        public ActionResult Modules()
        {

				return View ();

        }
        #endregion

		#region Calendars
		public ActionResult Calendars()
		{

			return View ();

		}
		#endregion
		#region Banners
        public ActionResult Banners()
        {
            return View();
        }
        #endregion
		#region Address
		public ActionResult Address()
		{

			return View ();

		}
		#endregion
		#region Address
		public ActionResult Media()
		{

			return View ();

		}
		#endregion
    }
}
