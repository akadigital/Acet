﻿using ACET_WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using ExtensionMethods;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Web;
using System.IO;


namespace ACET_WEB { 
    public static class NoSymbol
    {

        [EdmFunction("acet_dbModel.Store", "fn_nosymbol")]
        public static string fn_nosymbol(string input_string)
        {
            throw new NotSupportedException("Direct calls are not supported.");
        }
    }
}
namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static string MakeIndexData(this string text)
        {
            string noHTML = Regex.Replace(text, @"<[^>]+>|&nbsp;", "").Trim();
            return noHTML.NonUnicode().Replace("-", " ");
        }
        public static string NonUnicode(this string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",  
    "đ",  
    "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",  
    "í","ì","ỉ","ĩ","ị",  
    "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",  
    "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",  
    "ý","ỳ","ỷ","ỹ","ỵ",".",","," ","&","?","/","\\","!","(",")","\"", "'","“","”",":"};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",  
    "d",  
    "e","e","e","e","e","e","e","e","e","e","e",  
    "i","i","i","i","i",  
    "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",  
    "u","u","u","u","u","u","u","u","u","u","u",  
    "y","y","y","y","y","","","-","-","-","-","-","","","","","","","",""};
            text = text.ToLower();
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                //text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            text = text.Replace("---", "-");
            text = text.Replace("--", "-");
            return text;
        }  
    }
}
namespace ACET_WEB.Apis
{
    public class ArticlesController : ApiController
    {
        private string getLang()
        {
            string lang = "vi";
            HttpCookie cookie = HttpContext.Current.Request.Cookies["curlang"];
            if (cookie != null)
            {
                lang = cookie.Value;
            }
            return lang;
        }
		private void deleteAllMeta(int postID){
			using (SimpleContext ct = new SimpleContext ()) {
				var posts = ct.PostMetas.Where (p => p.PostId == postID).ToList();
				ct.PostMetas.RemoveRange (posts);
				ct.SaveChanges ();
			}
		}
		[Authorize(Roles = "Administrator")]
		public Boolean Post(int id) {
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{   
					var u = dc.Posts.FirstOrDefault(c => c.Id.Equals(id));
					deleteAllMeta(u.Id);
					dc.Posts.DeleteObject(u);
					dc.SaveChanges();
					return true;
				}
				catch (Exception ex)
				{
					throw ex;
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
				}
			}
		}
		public WrapUserSerial GetList([FromUri] int cus_type, [FromUri] int offset, [FromUri] int limit, [FromUri] string search_value="", [FromUri] string clang="")
        {
            if (search_value!=null){
                if (search_value.Trim() != "")
                {
                    search_value = search_value.NonUnicode().Replace("-"," ").Replace("  "," ");
                }
            }
            
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var rs = new WrapUserSerial();
                var rs_data = new List<List<dynamic>>();
				var lang = "";
				if (clang != "") {
					lang = clang;
				} else {
					lang = getLang();
				}
                
                if (lang.Equals("vi"))
                {
					if (cus_type == 4)
					{
						var posts = dc.Media.Where(p => p.MediaCategory.Id ==12).OrderByDescending(p => p.Created);
						foreach (var post in posts)
						{
							var item = new List<dynamic>();
							item.Add(post.Title);
							item.Add(post.Link);
							item.Add(post.MediaCategory.Name);
							item.Add(post.Created.ToString());
							item.Add(post.MediaCategory.Id);
							item.Add(post.MediaCategory.Img);
							item.Add(post.Id);
							rs_data.Add(item);
						}
						rs.draw = 0;
						rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = rs_data.Count();
						rs.data = rs_data;
						return rs;
					}
                    if (cus_type == 2)
                    {
						var posts = dc.MediaCategories.Where (p => p.Status == 2).OrderByDescending (p => p.Created);
                        foreach (var post in posts)
                        {
                            
							DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("/Uploads") + "\\" + post.Img);//Assuming Test is your Folder
							FileInfo[] Files = d.GetFiles("*.*"); //Getting Text files
							int i=0;
							foreach(FileInfo file in Files )
							{
								i++;
								var filel = file.Name.Split ('.');
								var item = new List<dynamic>();
								item.Add(filel[0]);
								item.Add("/Uploads/"+ post.Img + "/" + file.Name);
								item.Add(post.Name);
								item.Add(post.Created.ToString());
								item.Add (post.Id);
								item.Add ("/Uploads/"+ post.Img + "/" + Files[0].Name);
								item.Add (i);
								rs_data.Add(item);
							}

                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
                        rs.recordsTotal = rs_data.Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 3)
                    {
                        var posts = dc.Media.Where(p => p.MediaCategory.IsHome == true && p.Type.Equals("video")).OrderByDescending(p => p.Created);
                        foreach (var post in posts)
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Title);
                            item.Add(post.Link);
                            item.Add(post.MediaCategory.Name);
                            item.Add(post.Created.ToString());
                            item.Add(post.MediaCategory.Id);
                            item.Add(post.MediaCategory.Img);
                            item.Add(post.Id);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
                        rs.recordsTotal = rs_data.Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 1)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76)).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));

                        posts = posts.OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(offset).Take(limit);

                        var post_list = posts.ToArray();
                        var total = post_list.Count();
                        //if (total < limit && total > 0)
                        //{
                        //    offset = offset + total - limit;
                        //    if (offset < 0)
                        //    {
                        //        offset = 0;
                        //    }
                        //    posts = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76)).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(offset).Take(limit);
                        //    post_list = posts.ToArray();
                        //}
                        foreach (var post in post_list)
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            item.Add(post.Desc);
                            item.Add(post.Content);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76)).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 5)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 92).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(offset).Take(limit);
                        var post_list = posts.ToArray();
                        var total = post_list.Count();

                        foreach (var post in post_list)
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            item.Add(post.Desc);
                            item.Add(post.Content);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 92).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 72)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1))).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && p.ContentIndex.ToLower().Contains(search_value.ToLower())&& (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1)));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 72).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 73)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && p.CategoryId == 73 && p.ContentIndex.ToLower().Contains(search_value.ToLower()));
                            }
                        }

                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 90)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.Status == 1 && p.CategoryId == 90).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
                                posts = posts.Where(p => p.Status == 1 && p.ContentIndex.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.Status == 1 && p.CategoryId == 90).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 74)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 74).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
                                posts = posts.Where(p => p.Status == 1 && p.ContentIndex.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.Status == 1 && p.CategoryId == 74).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 94)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 94).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
                                posts = posts.Where(p => p.Status == 1 && p.ContentIndex.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 94).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 95)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 95).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals(""));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
                                posts = posts.Where(p => p.Status == 1 && p.ContentIndex.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.Slug);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 95).Where(p => p.Title!=null && p.Content!=null && !p.Title.Equals("")).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 10)
                    {
						var posts = dc.Posts.Where(p => p.Content != null && p.Title != null && p.CategoryId!=100 && p.CategoryId!=101 && p.CategoryId!=102 && !p.Slug.Equals("")).Where(p => p.Status==1);
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
                                posts = posts.Where(p => p.Status == 1 && p.ContentIndex.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        var total = posts.Count();
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
							if (post.Image == null) {
								post.Image = "/home/img/logo.png";
							}
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.Title);
                            var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.Slug);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
							if (post.CategoryId == 75 || post.CategoryId == 76 || post.CategoryId == 92) {
								item.Add(post.Category.Category1.Slug);
							} else {
								item.Add(post.Category.Slug);
							}
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
                        rs.recordsTotal = total;
                        rs.data = rs_data;
                        return rs;
                    }
                    else
                    {
						var posts = dc.Posts.Where (p => p.Content != null && p.Title != null && p.CategoryId == cus_type && p.Status==1).Where (p => p.Title != null && p.Content != null && !p.Title.Equals (""));
						if (cus_type == 100) {
							posts =	posts.OrderBy (p => p.Title).ThenByDescending (p => p.Created).Skip (offset).Take (limit);
						} else {
							posts = posts.OrderByDescending(p=>p.order).ThenByDescending(p=>p.Modified).Skip(offset).Take(limit);
						}
							
						foreach (var post in posts.ToArray())
						{
							if (post.Image == null) {
								post.Image = "/home/img/logo.png";
							}
							var item = new List<dynamic>();
							item.Add(post.Id.ToString());
							item.Add(post.Title);
							var clearContent = Regex.Replace(post.Content, "<[^>]*(>|$)", string.Empty);
							var descByContent = clearContent;
							item.Add(string.IsNullOrEmpty(post.Desc) ? descByContent : post.Desc);
							item.Add(post.Image);
							item.Add(post.Created.ToString());
							item.Add(post.Slug);
							item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
							item.Add(post.Type);
							item.Add(post.Category.Slug);
							item.Add (post.Content);
							rs_data.Add(item);
						}
						rs.draw = 0;
						rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Content != null && p.Title != null && p.CategoryId==cus_type).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                }
                else
                {
					if (cus_type == 4)
					{
						var posts = dc.Media.Where(p => p.MediaCategory.Id ==12).OrderByDescending(p => p.Created);
						foreach (var post in posts)
						{
							var item = new List<dynamic>();
							item.Add(post.TitleEng);
							item.Add(post.Link);
							item.Add(post.MediaCategory.NameEng);
							item.Add(post.Created.ToString());
							item.Add(post.MediaCategory.Id);
							item.Add(post.MediaCategory.Img);
							item.Add(post.Id);
							rs_data.Add(item);
						}
						rs.draw = 0;
						rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = rs_data.Count();
						rs.data = rs_data;
						return rs;
					}
					if (cus_type == 2)
					{
						var posts = dc.MediaCategories.Where (p => p.Status == 2).OrderByDescending (p => p.Created);
						foreach (var post in posts)
						{

							DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("/Uploads") + "\\" + post.Img);//Assuming Test is your Folder
							FileInfo[] Files = d.GetFiles("*.*"); //Getting Text files
							int i=0;
							foreach(FileInfo file in Files )
							{
								i++;
								var filel = file.Name.Split ('.');
								var item = new List<dynamic>();
								item.Add(filel[0]);
								item.Add("/Uploads/"+ post.Img + "/" + file.Name);
								item.Add(post.NameEng);
								item.Add(post.Created.ToString());
								item.Add (post.Id);
								item.Add ("/Uploads/"+ post.Img + "/" + Files[0].Name);
								item.Add (i);
								rs_data.Add(item);
							}

						}
						rs.draw = 0;
						rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = rs_data.Count();
						rs.data = rs_data;
						return rs;
					}
					if (cus_type == 3)
					{
						var posts = dc.Media.Where(p => p.MediaCategory.IsHome == true && p.Type.Equals("video")).OrderByDescending(p => p.Created);
						foreach (var post in posts)
						{
							var item = new List<dynamic>();
							item.Add(post.TitleEng);
							item.Add(post.Link);
							item.Add(post.MediaCategory.NameEng);
							item.Add(post.Created.ToString());
							item.Add(post.MediaCategory.Id);
							item.Add(post.MediaCategory.Img);
							item.Add(post.Id);
							rs_data.Add(item);
						}
						rs.draw = 0;
						rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = rs_data.Count();
						rs.data = rs_data;
						return rs;
					}
                    if (cus_type == 1)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76) && (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));

                        posts = posts.OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(offset).Take(limit);

                        var post_list = posts.ToArray();
                        var total = post_list.Count();
                        //if (total < limit && total > 0)
                        //{
                        //    offset = offset + total - limit;
                        //    if (offset < 0)
                        //    {
                        //        offset = 0;
                        //    }
                        //    posts = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76)).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(offset).Take(limit);
                        //    post_list = posts.ToArray();
                        //}
                        foreach (var post in post_list)
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            item.Add(post.DescEng);
                            item.Add(post.ContentEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76) && (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 5)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 92 && (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(offset).Take(limit);
                        var post_list = posts.ToArray();
                        var total = post_list.Count();

                        foreach (var post in post_list)
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            item.Add(post.DescEng);
                            item.Add(post.ContentEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 92&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 72)
                    {
						var posts = dc.Posts.Where(p => (p.Status == 1 && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1)))&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()) && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1))&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 72&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 73)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()));
                            }
                        }

                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 90)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.Status == 1 && p.CategoryId == 90&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.Status == 1 && p.CategoryId == 90&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 74)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 74 && (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1  && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.Status == 1 && p.CategoryId == 74&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 94)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 94&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")) && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")) && p.CategoryId == 94).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 95)
                    {
						var posts = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 95&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")));
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals("")) && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
                            item.Add(post.Category.SlugEng);
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 95&& (p.TitleEng!= null && p.ContentEng!=null && !p.TitleEng.Equals(""))).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                    if (cus_type == 10)
                    {
						var posts = dc.Posts.Where(p => p.ContentEng != null && p.TitleEng != null&& p.CategoryId!=100 && p.CategoryId!=101 && p.CategoryId!=102 && !p.SlugEng.Equals("")).Where(p => p.Status==1);
                        if (search_value != null)
                        {
                            if (!search_value.Trim().Equals(""))
                            {
								posts = posts.Where(p => p.Status == 1 && p.ContentIndexEnd.ToLower().Contains(search_value.ToLower()));
                            }
                        }
                        var total = posts.Count();
                        posts = posts.OrderByDescending(p => p.Created).Skip(offset).Take(limit);
                        foreach (var post in posts.ToArray())
                        {   
							if (post.Image == null) {
								post.Image = "/home/img/logo.png";
							}
                            var item = new List<dynamic>();
                            item.Add(post.Id.ToString());
                            item.Add(post.TitleEng);
                            var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
                            var descByContent = clearContent;
                            item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
                            item.Add(post.Image);
                            item.Add(post.Created.ToString());
                            item.Add(post.SlugEng);
                            item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
                            item.Add(post.Type);
							if (post.CategoryId == 75 || post.CategoryId == 76 || post.CategoryId == 92) {
								item.Add(post.Category.Category1.SlugEng);
							} else {
								item.Add(post.Category.SlugEng);
							}
                            
                            rs_data.Add(item);
                        }
                        rs.draw = 0;
                        rs.recordsFiltered = rs_data.Count();
                        rs.recordsTotal = total;
                        rs.data = rs_data;
                        return rs;
                    }
                    else
                    {
						var posts = dc.Posts.Where (p => p.ContentEng != null && p.TitleEng != null && p.CategoryId == cus_type && p.Status==1);
						if (cus_type == 100) {
							posts = posts.OrderBy(p=>p.TitleEng).ThenByDescending(p=>p.Modified).Skip(offset).Take(limit);
						} else {
							posts = posts.OrderByDescending(p=>p.order).ThenByDescending(p=>p.Modified).Skip(offset).Take(limit);
						}
						foreach (var post in posts.ToArray())
						{
							if (post.Image == null) {
								post.Image = "/home/img/logo.png";
							}
							var item = new List<dynamic>();
							item.Add(post.Id.ToString());
							item.Add(post.TitleEng);
							var clearContent = Regex.Replace(post.ContentEng, "<[^>]*(>|$)", string.Empty);
							var descByContent = clearContent;
							item.Add(string.IsNullOrEmpty(post.DescEng) ? descByContent : post.DescEng);
							item.Add(post.Image);
							item.Add(post.Created.ToString());
							item.Add(post.SlugEng);
							item.Add(post.Location != null ? post.Location.Name : "ACET Việt Nam");
							item.Add(post.Type);
							item.Add(post.Category.SlugEng);
							item.Add(post.ContentEng);
							rs_data.Add(item);
						}
                        rs.draw = 0;
						rs.recordsFiltered = rs_data.Count();
						rs.recordsTotal = dc.Posts.Where(p => p.ContentEng != null && p.TitleEng != null && p.CategoryId==cus_type).Count();
                        rs.data = rs_data;
                        return rs;
                    }
                }
                
            }
        }
        public WrapUserSerial GetAll([FromUri] int draw, [FromUri] int length, [FromUri] int start,
           [FromUri] string search_value, [FromUri] string order_colunm, [FromUri] string order_dir, [FromUri] string cus_type)
        {

            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var rs = new WrapUserSerial();
                var rs_data = new List<List<dynamic>>();
				var users = dc.Posts.Where(c => c.Type.Equals(cus_type) && c.Status > -1);
                if (search_value != null)
                {
                    if (!search_value.Trim().Equals(""))
                    {
                        users = users.Where(c => c.Title.Contains(search_value) || c.TitleEng.Contains(search_value));
                    }
                }
                switch (order_colunm)
                {
                    case "0":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }
                        break;
                    case "1":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Title);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Title);
                        }
                        break;
                    case "2":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.TitleEng);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.TitleEng);
                        }
                        break;
                    case "3":
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Modified);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Modified);
                        }
                        break;
                    default:
                        if (order_dir.Equals("asc"))
                        {
                            users = users.OrderBy(c => c.Id);
                        }
                        else
                        {
                            users = users.OrderByDescending(c => c.Id);
                        }

                        break;
                }
                var recordsTotal = users.Count();
                users = users.Skip(start).Take(length);
                foreach (var user in users.ToArray())
                {
                    var item = new List<dynamic>();
                    item.Add(user.Id.ToString());
                    item.Add(user.Title);
                    item.Add(user.TitleEng);
                    item.Add(user.Modified.ToString());
					item.Add (user.Status);
                    rs_data.Add(item);
                }
                rs.draw = draw;
                rs.recordsFiltered = recordsTotal;
                rs.recordsTotal = recordsTotal;
                rs.data = rs_data;
                return rs;
            }
        }

        
    }
}
