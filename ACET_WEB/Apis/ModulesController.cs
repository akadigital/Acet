﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using ACET_WEB.Models;
using ACET_WEB.App_Start;

namespace ACET_WEB.Apis
{
	public class ModuleForm {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
		public int Order { get; set; }
		public string Position { get; set; }
	}
	[Authorize(Roles = "Administrator")]
	public class ModulesController : ApiController
	{
		public WrapUserSerial GetAllUsers([FromUri] int draw, [FromUri] int length, [FromUri] int start,
			[FromUri] string search_value, [FromUri] string order_colunm, [FromUri] string order_dir)
		{

			using (acet_dbEntities dc = new acet_dbEntities())
			{
				var rs = new WrapUserSerial();
				var rs_data = new List<List<dynamic>>();
				var users = dc.Modules.Where(p=> p.Position.Equals("Page Bottom English") || p.Position.Equals("Page Bottom") 
					|| p.Position.Equals("Home Bottom English") || p.Position.Equals("Home Bottom") || p.Position.Equals("Home Right") 
					|| p.Position.Equals("Home Left") || p.Position.Equals("Home Right English") || p.Position.Equals("Home Left English") 
					|| p.Position.Equals("Page Right") ||p.Position.Equals("Page Right English") ||p.Position.Equals("Home About") 
					||p.Position.Equals("Home About English") ||p.Position.Equals("Home 6 Block") ||p.Position.Equals("Home 6 Block English") 
					||p.Position.Equals("Home Introduction") ||p.Position.Equals("Home Introduction English") ||p.Position.Equals("Home Address") 
					||p.Position.Equals("Home Address English")||p.Position.Equals("Contact Hotline HN")||p.Position.Equals("Contact Hotline HCM")
					||p.Position.Equals("Header Tag") ||p.Position.Equals("Body Tag")||p.Position.Equals("AE Box")||p.Position.Equals("IELTS Box")
					||p.Position.Equals("FS Box")||p.Position.Equals("AE Box English")||p.Position.Equals("FS Box English")
					||p.Position.Equals("IELTS Box English")||p.Position.Equals("CenterBar English")||p.Position.Equals("CenterBar") || p.Position.Equals("TKB Script")); 
				
				if (search_value!=null)
				{
					if (!search_value.Trim().Equals(""))
					{
						users = users.Where(c => c.Value.Contains(search_value) || c.Name.Contains(search_value));
					}
				}
				switch (order_colunm)
				{
				case "0":
					if (order_dir.Equals("asc"))
					{
						users = users.OrderBy(c => c.Id);
					}
					else
					{
						users = users.OrderByDescending(c => c.Id);
					}
					break;
				case "1":
					if (order_dir.Equals("asc"))
					{
						users = users.OrderBy(c => c.Name);
					}
					else
					{
						users = users.OrderByDescending(c => c.Name);
					}
					break;
				
				default:
					if (order_dir.Equals("asc"))
					{
						users = users.OrderBy(c => c.Id);
					}
					else
					{
						users = users.OrderByDescending(c => c.Id);    
					}

					break;
				}
				var filter = users;
				filter = filter.Skip(start).Take(length);
				foreach (var user in filter.ToArray())
				{
					var item = new List<dynamic>();
					item.Add(user.Id.ToString());
					item.Add(user.Name);
					item.Add(user.Position);
					item.Add(user.Modified);
					item.Add(user.Order);
					rs_data.Add(item);
				}
				rs.draw = draw;
				rs.recordsFiltered = users.Count();
				rs.recordsTotal = users.Count();
				rs.data = rs_data;
				return rs;
			}
		}
		public Module Get(int id)
		{
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				var user = dc.Modules.FirstOrDefault(c => c.Id.Equals(id));
				if (user == null)
				{
					throw new HttpResponseException(HttpStatusCode.NotFound);
				}
				else {
					
					return user;
				}
			}
		}

		public Boolean Post(int id) {
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{   
					var u = dc.Modules.FirstOrDefault(c => c.Id.Equals(id));

					dc.Modules.DeleteObject(u);
					dc.SaveChanges();
					return true;
				}
				catch (Exception ex)
				{
					throw ex;
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
				}
			}
		}
		public Boolean Post([FromBody] ModuleForm userform)
		{
			using (acet_dbEntities dc = new acet_dbEntities())
			{
				try
				{
					if (userform.Id>0)
					{
						var u = dc.Modules.FirstOrDefault(c => c.Id.Equals(userform.Id));
						u.Modified = DateTime.Now;
						u.Name = userform.Name;
						u.Value = userform.Value;
						u.Order = userform.Order;
						u.Position = userform.Position;
						dc.SaveChanges();
						return true;
					}
					else
					{
						Module u = new Module();
						u.Name = userform.Name;
						u.Value = userform.Value;
						u.Order = userform.Order;
						u.Created = DateTime.Now;
						u.Modified = DateTime.Now;
						u.Position = userform.Position;

						dc.Modules.AddObject(u);
						dc.SaveChanges();
						return true;
					}  
				}
				catch (Exception ex)
				{
					throw ex;
					//throw new HttpResponseException(HttpStatusCode.BadRequest);
				}   
			}
		}
	}
}

