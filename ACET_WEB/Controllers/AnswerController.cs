﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Controllers
{
	[Authorize(Roles = "Administrator")]
    public class AnswerController : Controller
    {
        private acet_dbEntities db = new acet_dbEntities();

        //
        // GET: /Answer/

        public ActionResult Index(int question=0)
        {
            ViewBag.gid = question;
            var answers = db.Answers.Include("Question");
            if (db.Questions.FirstOrDefault(p => p.Id == question) != null)
            {
                return View(answers.Where(q => q.QuestionId == question).ToList());
            }
            return View(answers.ToList());
        }

        //
        // GET: /Answer/Details/5

        public ActionResult Details(int id = 0)
        {
            Answer answer = db.Answers.Single(a => a.Id == id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            return View(answer);
        }

        //
        // GET: /Answer/Create

        public ActionResult Create(int question=0)
        {
            ViewBag.gid = question;
            var qg = db.Questions.FirstOrDefault(p => p.Id == question);
            if (qg != null)
            {
                ViewBag.QuestionId = new SelectList(db.Questions, "Id", "Question1", qg.Id);
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "Id", "Question1");
            return View();
        }

        //
        // POST: /Answer/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Answers.AddObject(answer);
                db.SaveChanges();
                return RedirectToAction("Index", new  { question=answer.QuestionId});
            }

            ViewBag.QuestionId = new SelectList(db.Questions, "Id", "Question1", answer.QuestionId);
            return View(answer);
        }

        //
        // GET: /Answer/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Answer answer = db.Answers.Single(a => a.Id == id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "Id", "Question1", answer.QuestionId);
            return View(answer);
        }

        //
        // POST: /Answer/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Answers.Attach(answer);
                db.ObjectStateManager.ChangeObjectState(answer, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index", new { question = answer.QuestionId });
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "Id", "Question1", answer.QuestionId);
            return View(answer);
        }

        //
        // GET: /Answer/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Answer answer = db.Answers.Single(a => a.Id == id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            return View(answer);
        }

        //
        // POST: /Answer/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Answer answer = db.Answers.Single(a => a.Id == id);
            var question = answer.QuestionId;
            db.Answers.DeleteObject(answer);
            db.SaveChanges();
            return RedirectToAction("Index", new { question = question });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}