﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ACET_WEB
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.MapRoute(
				name: "sitemap-index",
				url: "sitemap.xml",
				defaults: new { lang = "vi", controller = "Home", action = "Sitemap2"}
			);

			routes.MapRoute(
				name: "rss-news",
				url: "{slug}/rss",
				defaults: new { lang = "vi", controller = "Home", action = "RSS"}
			);


			routes.MapRoute(
				name: "sitemap-post",
				url: "sitemap-post-{page}.xml",
				defaults: new { lang = "vi", controller = "Home", action = "Sitemap" }
			);
			routes.MapRoute(
				name: "sitemap-category",
				url: "sitemap-category.xml",
				defaults: new { lang = "vi", controller = "Home", action = "SitemapCategory" }
			);


            #region Course
            routes.MapRoute(
             name: "luyen-thi-ielts",
             url: "luyen-thi-ielts.html",
             defaults: new { lang = "vi", controller = "Home", action = "CourseDetail", id = "luyen-thi-ielts", pid = "tai-sao-chon-ielts" }
          );
            routes.MapRoute(
              name: "anh-ngu-hoc-thuat",
              url: "anh-ngu-hoc-thuat.html",
              defaults: new { lang = "vi", controller = "Home", action = "CourseDetail", id = "anh-ngu-hoc-thuat", pid = "tai-sao-chon-anh-ngu-hoc-thuat" }
           );
            routes.MapRoute(
             name: "first-step-old",
             url: "first-steps.html",
                defaults: new { lang = "vi", controller = "Home", action = "CourseDetail_old", id = "first-steps", pid = "tai-sao-chon-first-steps" }
          );
            routes.MapRoute(
             name: "first-step",
             url: "tieng-anh-thcs-first-steps.html",
				defaults: new { lang = "vi", controller = "Home", action = "CourseDetail", id = "first-steps", pid = "tai-sao-chon-first-steps" }
          );

            routes.MapRoute(
           name: "luyen-thi-ielts-en",
				url: "ielts-preparation.html",
				defaults: new { lang = "en", controller = "Home", action = "CourseDetail", id = "ielts-preparation", pid = "why-ielts-preparation" }
        );
            routes.MapRoute(
              name: "anh-ngu-hoc-thuat-en",
              url: "academic-english.html",
				defaults: new { lang = "en", controller = "Home", action = "CourseDetail", id = "academic-english", pid = "why-academic-english" }
           );
            routes.MapRoute(
             name: "first-step-en",
             url: "first-steps-course.html",
				defaults: new { lang = "en", controller = "Home", action = "CourseDetail", id = "first-steps-course", pid = "why-first-steps" }
          );
            #endregion

			routes.MapRoute(
				name: "search-result-page",
				url: "tim-kiem.html",
				defaults: new { lang = "vi", controller = "Home", action = "searchresult" }
			);
			routes.MapRoute(
				name: "search-result-page-en",
				url: "search.html",
				defaults: new { lang = "en", controller = "Home", action = "searchresult" }
			);



			routes.MapRoute(
				name: "test-online-page",
				url: "online-test.html",
				defaults: new { lang = "en", controller = "Home", action = "testonline" }
			);
			routes.MapRoute(
				name: "test-online-page-en",
				url: "kiem-tra-tieng-anh-truc-tuyen.html",
				defaults: new { lang = "vi", controller = "Home", action = "testonline" }
			);


            routes.MapRoute(
               name: "cam-on",
               url: "cam-on.html",
               defaults: new { lang = "vi", controller = "Home", action = "Thankyou" }
           );
            routes.MapRoute(
              name: "cam-on-en",
              url: "thank-you.html",
              defaults: new { lang = "en", controller = "Home", action = "Thankyou" }
          );
            routes.MapRoute(
                name: "download",
                url: "tai-tai-lieu-luyen-thi-ielts-mien-phi.html",
                defaults: new { lang = "vi", controller = "Home", action = "Download" }
            );
            routes.MapRoute(
                name: "download-en",
                url: "free-ielts-documents.html",
                defaults: new { lang = "en", controller = "Home", action = "Download" }
            );
            routes.MapRoute(
                name: "downloaddetail",
                url: "download/tai-tai-lieu-luyen-thi-ielts-mien-phi.html",
                defaults: new { lang = "vi", controller = "Home", action = "DownloadDetail" }
            );
            routes.MapRoute(
               name: "downloaddetail-en",
               url: "download/free-ielts-documents.html",
               defaults: new { lang = "en", controller = "Home", action = "DownloadDetail" }
           );
            routes.MapRoute(
               name: "dk-khoa-hoc",
               url: "dang-ky-khoa-hoc.html",
               defaults: new { lang = "vi", controller = "Home", action = "RegisterAll" }
           );
            routes.MapRoute(
              name: "dk-khoa-hoc-en",
              url: "course-register.html",
              defaults: new { lang = "en", controller = "Home", action = "RegisterAll" }
          );
            routes.MapRoute(
              name: "thoi-khoa-bieu",
              url: "thoi-khoa-bieu.html",
              defaults: new { lang = "vi", controller = "Home", action = "TKB" }
          );
            routes.MapRoute(
                name: "thoi-khoa-bieu-en",
                url: "timetable.html",
                defaults: new { lang = "en", controller = "Home", action = "TKB" }
            );

            routes.MapRoute(
                name: "tuyen-dung",
                url: "tuyen-dung.html",
                defaults: new { lang = "vi", controller = "Home", action = "Recruitment" }
            );
            routes.MapRoute(
               name: "tuyen-dung-en",
               url: "recruitment.html",
               defaults: new { lang = "en", controller = "Home", action = "Recruitment" }
           );
            routes.MapRoute(
               name: "tuyen-dung-giang-vien",
               url: "tuyen-dung/giang-vien.html",
               defaults: new { lang = "vi", controller = "Home", action = "Recruitment" }
           );
            routes.MapRoute(
               name: "tuyen-dung-giang-vien-en",
               url: "recruitment/academic.html",
               defaults: new { lang = "en", controller = "Home", action = "Recruitment" }
           );
            routes.MapRoute(
               name: "tuyen-dung-nhan-vien",
               url: "tuyen-dung/nhan-vien.html",
               defaults: new { lang = "vi", controller = "Home", action = "Recruitment" }
           );
            routes.MapRoute(
              name: "tuyen-dung-nhan-vien-en",
              url: "recruitment/non-academic.html",
              defaults: new { lang = "en", controller = "Home", action = "Recruitment" }
          );
            routes.MapRoute(
               name: "tuyen-dung-nhan-vien-detail",
               url: "tuyen-dung/nhan-vien/{id}.html",
               defaults: new { lang = "vi", controller = "Home", action = "RecruitmentDetail"}
           );
            routes.MapRoute(
             name: "tuyen-dung-nhan-vien-detail-en",
             url: "recruitment/non-academic/{id}.html",
             defaults: new { lang = "en", controller = "Home", action = "RecruitmentDetail"}
         );

            routes.MapRoute(
                name: "tuyen-dung-giang-vien-detail",
                url: "tuyen-dung/giang-vien/{id}.html",
                defaults: new { lang = "vi", controller = "Home", action = "RecruitmentDetail" }
            );
            routes.MapRoute(
               name: "tuyen-dung-giang-vien-detail-en",
               url: "recruitment/academic/{id}.html",
               defaults: new { lang = "en", controller = "Home", action = "RecruitmentDetail" }
           );
           
            routes.MapRoute(
              name: "hoc-bong",
              url: "hoc-bong.html",
              defaults: new { lang = "vi", controller = "Home", action = "HocBong" }
          );
            routes.MapRoute(
             name: "hoc-bong-en",
             url: "scholarship.html",
             defaults: new { lang = "en", controller = "Home", action = "HocBong" }
         );
            routes.MapRoute(
             name: "hoc-bong-idp",
             url: "hoc-bong-idp.html",
             defaults: new { lang = "vi", controller = "Home", action = "HocBongIDP" }
         );
            routes.MapRoute(
            name: "hoc-bong-idp-en",
            url: "idp-scholarship.html",
            defaults: new { lang = "en", controller = "Home", action = "HocBongIDP" }
        );
         
            routes.MapRoute(
              name: "lien-he",
              url: "lien-he.html",
              defaults: new { lang = "vi", controller = "Home", action = "Contact" }
            );
            routes.MapRoute(
             name: "lien-he-en",
             url: "contact-us.html",
             defaults: new { lang = "en", controller = "Home", action = "Contact" }
           );
            routes.MapRoute(
                name: "PostDetail",
                url: "{postTitle}_{id}.html",
                defaults: new {  controller = "Home", action = "News", postTitle = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "CourseDetail",
                url: "{id}/{pid}.html",
                defaults: new { controller = "Home", action = "CourseDetail" }
            );
            routes.MapRoute(
                name: "PostSlug",
                url: "{slug}.html",
                defaults: new { controller = "Home", action = "PostSlug" }
            );
			routes.MapRoute(
				name: "Review",
				url: "review",
				defaults: new { controller = "Home", action = "Review" }
			);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
           
        }
    }
}