﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACET_WEB.Models
{
    public class CateTree
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool HasChild { get; set; }
        public DateTime Modified { get; set; }
        public List<CateTree> Childs { get; set; }
       
    }
}