﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ACET_WEB.Apis
{
    public class EmailController : ApiController
    {
        public string Get([FromUri] string email)
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var u = dc.Users.FirstOrDefault(c => c.Email.Equals(email.Trim()));
                if (u!=null) {
                    return "Email is exists";
                }
            }
            return "true";
        }
    }
    public class UsernameController : ApiController
    {
        public string Get([FromUri] string username)
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var u = dc.Users.FirstOrDefault(c => c.UserName.Equals(username.Trim()));
                if (u != null)
                {
                    return "User Name is exists.";
                }
            }
            return "true";
        }
    }
}
