﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACET_WEB.Models
{
    public class LoginUsers
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}