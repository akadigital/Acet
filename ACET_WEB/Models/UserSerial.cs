﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACET_WEB.Models
{
    public class UserSerial
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Modified { get; set; }
        public int IsActive { get; set; }
        public string Email { get; set; }
        public List<int> Roles { get; set; }
    }
    public class WrapUserSerial
    {
        public int draw { get; set; }
        public List<List<dynamic>> data { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
    public class WrapTestOnlineSerial
    {
        public int draw { get; set; }
        public List<List<List<dynamic>>> data { get; set; }
        public string groupQuestion { get; set; }
    }
}