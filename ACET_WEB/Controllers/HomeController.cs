﻿using ACET_WEB.App_Start;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace ACET_WEB.Controllers
{
    public class HomeController : Controller
    {
        private PostMeta getMeta(int postID, string name)
        {
            using (SimpleContext ct = new SimpleContext())
            {

                return ct.PostMetas.FirstOrDefault(p => p.Name.Equals(name) && p.PostId == postID);

            }
        }
        private string getLang()
        {
            string lang = "vi";
            HttpCookie cookie = Request.Cookies["curlang"];
            if (cookie != null)
            {
                lang = cookie.Value;
            }
            return lang;
        }

        private void setCookie(string lang)
        {
            var cookie = new HttpCookie("curlang", lang);
            cookie.Expires = DateTime.Now.AddDays(30);
            HttpContext.Response.Cookies.Set(cookie);
        }

        public ActionResult TestOnline(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    return Redirect("/online-test.html");
                }
                else
                {
                    return Redirect("/kiem-tra-tieng-anh-truc-tuyen.html");
                }
            }
            return View();
        }

        public ActionResult TestOnlineDone(string lang = "")
        {
            return View();
        }

        public ActionResult SearchResult(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    return Redirect("/search.html");
                }
                else
                {
                    return Redirect("/tim-kiem.html");
                }
            }
            var query = this.Request.QueryString["q"];
            using (acet_dbEntities dc = new acet_dbEntities())
            {

                DateTime dateTimeNow = DateTime.UtcNow;
                DateTime datePast30 = dateTimeNow.Date.AddDays(-60);
                if (lang.Equals("en"))
                {
                    ViewBag.banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Search Banner") && p.Status == 0).OrderBy(p => p.Role).ToList();
                    ViewBag.banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Search Banner Mobile") && p.Status == 0).OrderBy(p => p.Role).ToList();

                    var post_lasted = dc.Posts.Include("Category").Where(p => p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90).Where(p => p.Status == 1 && p.ContentEng != null && p.TitleEng != null && p.SlugEng != null && !p.SlugEng.Equals("")).OrderByDescending(p => p.Created).Take(5).ToList();
                    var post_hotest = dc.Posts.Include("Category").Where(p => p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90).Where(p => p.Status == 1 && p.ContentEng != null && p.TitleEng != null && p.SlugEng != null).Where(p => p.Created > datePast30 && p.Status == 1).OrderByDescending(p => p.View).Take(5).ToList();

                    ViewData["post_lasted"] = post_lasted;
                    ViewData["post_hotest"] = post_hotest;
                    if (query == null)
                    {
                        query = "";
                    }
                    ViewData["search_value"] = query;
                    return View();
                }
                else
                {
                    ViewBag.banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Search Banner") && p.Status == 1).OrderBy(p => p.Role).ToList();
                    ViewBag.banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Search Banner Mobile") && p.Status == 1).OrderBy(p => p.Role).ToList();

                    var post_lasted = dc.Posts.Include("Category").Where(p => p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90).Where(p => p.Status == 1).OrderByDescending(p => p.Created).Take(5).ToList();
                    var post_hotest = dc.Posts.Include("Category").Where(p => p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90).Where(p => p.Created > datePast30 && p.Status == 1).OrderByDescending(p => p.View).Take(5).ToList();

                    ViewData["post_lasted"] = post_lasted;
                    ViewData["post_hotest"] = post_hotest;
                    if (query == null)
                    {
                        query = "";
                    }
                    ViewData["search_value"] = query;
                    return View();
                }

            }

        }

        public ActionResult Index(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                if (this.Request.QueryString["hl"] != null)
                {
                    if (this.Request.QueryString["hl"].Equals("vi"))
                    {
                        ViewBag.banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner") && p.Status == 1).OrderBy(p => p.Role).ToList();
                        ViewBag.banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner Mobile") && p.Status == 1).OrderBy(p => p.Role).ToList();
                        ViewBag.mid_banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner 1") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile 1") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.home_about = dc.Modules.Where(m => m.Position.Equals("Home About")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_6block = dc.Modules.Where(m => m.Position.Equals("Home 6 Block")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_intro = dc.Modules.Where(m => m.Position.Equals("Home Introduction")).OrderBy(m => m.Order).FirstOrDefault();

                        var testimonial = dc.Posts.Where(p => p.Category.ParentId == 48 && p.Status == 1).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(7).ToArray();
                        ViewBag.tinuudai = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1))).Where(p => p.Title != null && p.Content != null && !p.Title.Equals("")).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        ViewBag.sukien = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73).Where(p => p.Title != null && p.Content != null && !p.Title.Equals("")).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        return View(testimonial);
                    }
                    else
                    {
                        ViewBag.banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner") && p.Status == 0).OrderBy(p => p.Role).ToList();
                        ViewBag.banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner Mobile") && p.Status == 0).OrderBy(p => p.Role).ToList();
                        ViewBag.mid_banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner 1") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile 1") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.home_about = dc.Modules.Where(m => m.Position.Equals("Home About English")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_6block = dc.Modules.Where(m => m.Position.Equals("Home 6 Block English")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_intro = dc.Modules.Where(m => m.Position.Equals("Home Introduction English")).OrderBy(m => m.Order).FirstOrDefault();

                        var testimonial = dc.Posts.Where(p => p.Category.ParentId == 48 && p.Status == 1 && (p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals(""))).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(7).ToArray();
                        ViewBag.tinuudai = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1))).Where(p => p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        ViewBag.sukien = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73 && (p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals(""))).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        return View(testimonial);
                    }

                }
                else
                {
                    if (getLang().Equals("vi"))
                    {
                        ViewBag.banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner") && p.Status == 1).OrderBy(p => p.Role).ToList();
                        ViewBag.banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner Mobile") && p.Status == 1).OrderBy(p => p.Role).ToList();
                        ViewBag.mid_banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner 1") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile 1") && p.Status == 1).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.home_about = dc.Modules.Where(m => m.Position.Equals("Home About")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_6block = dc.Modules.Where(m => m.Position.Equals("Home 6 Block")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_intro = dc.Modules.Where(m => m.Position.Equals("Home Introduction")).OrderBy(m => m.Order).FirstOrDefault();

                        var testimonial = dc.Posts.Where(p => p.Category.ParentId == 48 && p.Status == 1).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(7).ToArray();
                        ViewBag.tinuudai = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1))).Where(p => p.Title != null && p.Content != null && !p.Title.Equals("")).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        ViewBag.sukien = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73).Where(p => p.Title != null && p.Content != null && !p.Title.Equals("")).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();

                        return View(testimonial);
                    }
                    else
                    {
                        ViewBag.banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner") && p.Status == 0).OrderBy(p => p.Role).ToList();
                        ViewBag.banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Home Banner Mobile") && p.Status == 0).OrderBy(p => p.Role).ToList();
                        ViewBag.mid_banner_home = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner 1") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.mid_banner_home_mobile1 = dc.ChatAgents.Where(p => p.Email.Equals("Mid Home Banner Mobile 1") && p.Status == 0).OrderBy(p => p.Role).FirstOrDefault();
                        ViewBag.home_about = dc.Modules.Where(m => m.Position.Equals("Home About English")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_6block = dc.Modules.Where(m => m.Position.Equals("Home 6 Block English")).OrderBy(m => m.Order).FirstOrDefault();
                        ViewBag.home_intro = dc.Modules.Where(m => m.Position.Equals("Home Introduction English")).OrderBy(m => m.Order).FirstOrDefault();

                        var testimonial = dc.Posts.Where(p => p.Category.ParentId == 48 && p.Status == 1 && (p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals(""))).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(7).ToArray();
                        ViewBag.tinuudai = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 72 || (p.CategoryId == 94 && p.IsPromotion == 1))).Where(p => p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        ViewBag.sukien = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 73 && (p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals(""))).OrderByDescending(p => p.Created).Skip(0).Take(4).ToList();
                        return View(testimonial);
                    }
                }

            }

        }

        public ActionResult News(int id, string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var post = dc.Posts.Include("Location").FirstOrDefault(p => p.Id == id);
                if (post == null)
                {
                    return Redirect("/");
                }
                if (post.View == null)
                {
                    post.View = 0;

                }
                post.View += 1;
                dc.SaveChanges();
                var post_related = dc.Posts.Include("Location").Where(p => p.CategoryId == post.CategoryId && p.Id != post.Id && p.Status == 1).OrderByDescending(p => p.Created).Take(3).ToList();
                var post_lasted = dc.Posts.Include("Location").Where(p => (p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90) && p.Status == 1).OrderByDescending(p => p.Created).Take(5).ToList();
                DateTime dateTimeNow = DateTime.UtcNow;
                DateTime datePast30 = dateTimeNow.Date.AddDays(-60);
                var post_hotest = dc.Posts.Include("Location").Where(p => (p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90) && p.Status == 1).Where(p => p.Created > datePast30).OrderByDescending(p => p.View).Take(5).ToList();
                ViewData["post_hotest"] = post_hotest;
                ViewData["post_related"] = post_related;
                ViewData["post_lasted"] = post_lasted;

                return View("NewsDetail", post);
            }
        }

        private string get_cate_meta(int cateID, string metaName)
        {
            using (SimpleContext ct = new SimpleContext())
            {
                var cate_meta = ct.PostMetas.FirstOrDefault(pm => pm.PostId == cateID && pm.Name.Equals(metaName));
                if (cate_meta != null)
                {
                    return cate_meta.Value;
                }
                else
                {
                    return "";
                }
            }
        }

        public ActionResult PostSlug(string slug, string lang = "")
        {
            if (!lang.Equals(""))
            {
                setCookie(lang);
            }
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                DateTime dateTimeNow = DateTime.UtcNow;
                DateTime datePast30 = dateTimeNow.Date.AddDays(-60);

                var model = dc.Categories.FirstOrDefault(c => c.Slug.Equals(slug));
                var model_en = dc.Categories.FirstOrDefault(c => c.SlugEng.Equals(slug));
                if (model == null && model_en == null)
                {
                    return Redirect("/");
                }
                string clang = this.Request.QueryString["hl"];
                string ct = this.Request.QueryString["type"];
                if (clang != null)
                {
                    if (clang.Equals("en") && model != null)
                    {
                        if (ct != null)
                        {
                            return Redirect("/" + model.SlugEng + ".html?type=" + ct);
                        }
                        return Redirect("/" + model.SlugEng + ".html");
                    }
                    if (clang.Equals("vi") && model_en != null)
                    {
                        if (ct != null)
                        {
                            return Redirect("/" + model_en.Slug + ".html?type=" + ct);
                        }
                        return Redirect("/" + model_en.Slug + ".html");
                    }
                }
                ViewBag.ct = "ae-ielts";
                if (ct != null)
                {
                    if (ct.Equals("first-step"))
                    {
                        ViewBag.ct = ct;
                    }
                }
                if (model != null && model.Id == 103)
                {
                    ViewBag.lang = "vi";
                    setCookie("vi");
                    ViewData["cate_img"] = model.Img;
                    ViewData["img_link"] = string.IsNullOrEmpty(get_cate_meta(model.Id, "img_link")) ? model.Img : get_cate_meta(model.Id, "img_link");
                    ViewData["img_link_en"] = string.IsNullOrEmpty(get_cate_meta(model.Id, "img_link_en")) ? model.Img : get_cate_meta(model.Id, "img_link_en");
                    ViewData["cate_imgmobile"] = model.ImgMobile;
                    ViewBag.studyconner = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 103).Where(p => p.Title != null && p.Content != null && !p.Title.Equals("")).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(8).ToList();
                    return View("Studycorner", model);
                }
                if (model_en != null && model_en.Id == 103)
                {
                    ViewBag.lang = "en";
                    setCookie("en");
                    ViewData["cate_img"] = model_en.Img;
                    ViewData["img_link"] = get_cate_meta(model_en.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(model_en.Id, "img_link_en");
                    ViewData["cate_imgmobile"] = model_en.ImgMobile;
                    ViewBag.studyconner = dc.Posts.Where(p => p.Status == 1 && p.CategoryId == 103).Where(p => p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(8).ToList();
                    return View("Studycorner", model_en);
                }
                if (model != null && model.Id == 48)
                {
                    ViewBag.lang = "vi";
                    setCookie("vi");
                    ViewData["cate_img"] = model.Img;
                    ViewData["img_link"] = get_cate_meta(model.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(model.Id, "img_link_en");
                    ViewData["cate_imgmobile"] = model.ImgMobile;

                    ViewBag.anhngu = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76)).Where(p => p.Title != null && p.Content != null && !p.Title.Equals("")).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(8).ToList();
                    return View("Testimonial-2", model);
                }
                if (model_en != null && model_en.Id == 48)
                {
                    ViewBag.lang = "en";
                    setCookie("en");
                    ViewData["cate_img"] = model_en.Img;
                    ViewData["img_link"] = get_cate_meta(model_en.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(model_en.Id, "img_link_en");
                    ViewData["cate_imgmobile"] = model_en.ImgMobile;
                    ViewBag.anhngu = dc.Posts.Where(p => p.Status == 1 && (p.CategoryId == 75 || p.CategoryId == 76) && (p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals(""))).OrderByDescending(p => p.CusPoint).ThenByDescending(p => p.Created).Skip(0).Take(8).ToList();

                    return View("Testimonial-2", model_en);
                }

                if (model != null)
                {
                    var post_lasted = dc.Posts.Include("Category").Where(p => (p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90) && p.Status == 1).OrderByDescending(p => p.Created).Take(5).ToList();
                    var post_hotest = dc.Posts.Include("Category").Where(p => (p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90) && p.Status == 1).Where(p => p.Created > datePast30).OrderByDescending(p => p.View).Take(5).ToList();
                    ViewData["post_lasted"] = post_lasted;
                    ViewData["post_hotest"] = post_hotest;
                    ViewData["cate_img"] = model.Img;
                    ViewData["img_link"] = get_cate_meta(model.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(model.Id, "img_link_en");
                    ViewData["cate_imgmobile"] = model.ImgMobile;
                    ViewBag.lang = "vi";
                    setCookie("vi");
                    return View("News", model);
                }
                else
                {
                    var post_lasted = dc.Posts.Include("Category").Where(p => (p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90) && p.Status == 1 && p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).OrderByDescending(p => p.Created).Take(5).ToList();
                    var post_hotest = dc.Posts.Include("Category").Where(p => (p.CategoryId == 72 || p.CategoryId == 73 || p.CategoryId == 74 || p.CategoryId == 90) && p.Status == 1 && p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).Where(p => p.Created > datePast30).OrderByDescending(p => p.View).Take(5).ToList();
                    ViewData["post_lasted"] = post_lasted;
                    ViewData["post_hotest"] = post_hotest;

                    ViewData["cate_img"] = model_en.Img;
                    ViewData["img_link"] = get_cate_meta(model_en.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(model_en.Id, "img_link_en");
                    ViewData["cate_imgmobile"] = model_en.ImgMobile;
                    ViewBag.lang = "en";
                    setCookie("en");
                    return View("News", model_en);
                }




            }
        }

        public ActionResult Comingsoon(string lang = "")
        {
            return View();
        }

        public ActionResult CourseDetail(string id, string pid, string lang = "")
        {
            if (!lang.Equals(""))
            {
                setCookie(lang);
            }
            using (acet_dbEntities dc = new acet_dbEntities())
            {

                var posts = dc.Posts.Include("Category").Include("Location").Where(p => p.Category.Slug.Equals(id) || p.Category.Category1.Slug.Equals(id)).Where(p => p.Status > 0);
                var post_model = posts.FirstOrDefault(p => p.Slug.Equals(pid) && p.Content != null && p.Title != null && !p.Title.Equals(""));

                var posts_en = dc.Posts.Include("Category").Include("Location").Where(p => p.Category.SlugEng.Equals(id) || p.Category.Category1.SlugEng.Equals(id)).Where(p => p.Status > 0);
                var post_model_en = posts_en.FirstOrDefault(p => p.SlugEng.Equals(pid) && p.ContentEng != null);

                if (post_model == null && post_model_en == null)
                {
                    return Redirect("/");
                }
                string clang = this.Request.QueryString["hl"];
                string ct = this.Request.QueryString["type"];
                if (clang != null)
                {
                    if (clang.Equals("en") && post_model != null)
                    {
                        if (post_model.Category.ParentId != null)
                        {
                            if (post_model.Category.ParentId == 48)
                            {
                                if (ct != null)
                                {
                                    return Redirect("/" + post_model.Category.Category1.SlugEng + "/" + post_model.SlugEng + ".html?type=" + ct);
                                }
                                return Redirect("/" + post_model.Category.Category1.SlugEng + "/" + post_model.SlugEng + ".html");
                            }
                        }

                        return Redirect("/" + post_model.Category.SlugEng + "/" + post_model.SlugEng + ".html");
                    }
                    if (clang.Equals("vi") && post_model_en != null)
                    {
                        if (post_model_en.Category.ParentId != null)
                        {
                            if (post_model_en.Category.ParentId == 48)
                            {
                                if (ct != null)
                                {
                                    return Redirect("/" + post_model_en.Category.Category1.Slug + "/" + post_model_en.Slug + ".html?type=" + ct);
                                }
                                return Redirect("/" + post_model_en.Category.Category1.Slug + "/" + post_model_en.Slug + ".html");
                            }
                        }
                        return Redirect("/" + post_model_en.Category.Slug + "/" + post_model_en.Slug + ".html");
                    }
                }


                DateTime dateTimeNow = DateTime.UtcNow;
                DateTime datePast30 = dateTimeNow.Date.AddDays(-60);
                if (post_model != null)
                {
                    var metadesc = getMeta(post_model.Id, "metadesc");
                    var keywords = getMeta(post_model.Id, "keywords");
                    if (keywords != null)
                    {
                        ViewBag.keywords = keywords.Value;
                    }
                    if (metadesc != null)
                    {
                        ViewBag.metadesc = metadesc.Value;
                    }
                    ViewBag.lang = "vi";
                    ViewData["cate_img"] = post_model.Category.Img;
                    ViewData["cate_imgmobile"] = post_model.Category.ImgMobile;
                    ViewData["img_link"] = get_cate_meta(post_model.Category.Id, "img_link") != null ? get_cate_meta(post_model.Category.Id, "img_link") : post_model.Category.Img;
                    ViewData["img_link_en"] = get_cate_meta(post_model.Category.Id, "img_link_en");
                    if (post_model.CategoryId == 94)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;

                        dc.SaveChanges();
                        return View("HocBong", post_model);
                    }
                    if (post_model.CategoryId == 95)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;

                        dc.SaveChanges();
                        return View("HocBongIDP", post_model);
                    }
                    if (post_model.Category.ParentId == 48)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;

                        dc.SaveChanges();
                        var post_related = dc.Posts.Include("Category").Where(p => p.CategoryId == post_model.CategoryId && p.Status == 1).Where(p => p.Id != post_model.Id && p.Status == 1).OrderByDescending(p => p.View).Take(8).ToList();
                        ViewData["post_related"] = post_related;
                        ViewData["cate_img"] = post_model.Category.Category1.Img;
                        ViewData["cate_imgmobile"] = post_model.Category.Category1.ImgMobile;
                        ViewData["img_link"] = get_cate_meta(post_model.Category.Category1.Id, "img_link");
                        ViewData["img_link_en"] = get_cate_meta(post_model.Category.Category1.Id, "img_link_en");

                        return View("TestimonialDetail", post_model);
                    }
                    if (post_model.CategoryId == 103)
                    {

                        return View("TipsDetail", post_model);
                    }
                    if (post_model.Category.ParentId == 47)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;
                        dc.SaveChanges();
                        var post_related = dc.Posts.Include("Category").Include("Location").Where(p => p.CategoryId == post_model.CategoryId && p.Id != post_model.Id && p.Status == 1).OrderByDescending(p => p.Created).Take(3).ToList();
                        var post_lasted = dc.Posts.Include("Category").Include("Location").Where(p => (p.Category.ParentId == 47) && p.Status == 1).OrderByDescending(p => p.Created).Take(5).ToList();
                        var post_hotest = dc.Posts.Include("Category").Include("Location").Where(p => (p.Category.ParentId == 47) && p.Status == 1).Where(p => p.Created > datePast30).OrderByDescending(p => p.View).Take(5).ToList();
                        ViewData["post_hotest"] = post_hotest;
                        ViewData["post_related"] = post_related;
                        ViewData["post_lasted"] = post_lasted;

                        ViewData["cate_img"] = string.IsNullOrEmpty(get_cate_meta(post_model.Id, "banner")) ? post_model.Category.Img : get_cate_meta(post_model.Id, "banner");
                        ViewData["cate_imgmobile"] = string.IsNullOrEmpty(get_cate_meta(post_model.Id, "banner_m")) ? post_model.Category.ImgMobile : get_cate_meta(post_model.Id, "banner_m");
                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";

                        return View("NewsDetail", post_model);
                    }
                    if (post_model.CategoryId == 99)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;
                        dc.SaveChanges();
                        ViewData["cate_img"] = post_model.Image;
                        if (post_model.Image != null)
                        {
                            var t_ext = Path.GetExtension(post_model.Image.ToString());
                            int t_length = post_model.Image.ToString().Length;
                            var t_name = post_model.Image.Substring(0, t_length - t_ext.Length);
                            ViewData["cate_imgmobile"] = t_name + "_mobile" + t_ext;
                        }
                        else
                        {
                            ViewData["cate_imgmobile"] = null;
                        }

                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";
                        return View("PageDetail", post_model);
                    }
                    if (post_model.CategoryId == 104)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;
                        dc.SaveChanges();
                        ViewData["cate_img"] = post_model.Image;
                        if (post_model.Image != null)
                        {
                            var t_ext = Path.GetExtension(post_model.Image.ToString());
                            int t_length = post_model.Image.ToString().Length;
                            var t_name = post_model.Image.Substring(0, t_length - t_ext.Length);
                            ViewData["cate_imgmobile"] = t_name + "_mobile" + t_ext;
                        }
                        else
                        {
                            ViewData["cate_imgmobile"] = null;
                        }

                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";
                        return View("PageFull", post_model);
                    }
                    ViewData["posts"] = posts.OrderBy(c => c.order).ToList();
                    if (post_model.CategoryId == 45)
                    {
                        return View("PostAbout", post_model);
                    }
                    switch (post_model.CategoryId)
                    {
                        case 96:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("FS Box")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                        case 97:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("AE Box")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                        case 98:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("IELTS Box")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                    }
                    return View(post_model);
                }
                else
                {
                    var metadesc = getMeta(post_model_en.Id, "metadesc");
                    var keywords = getMeta(post_model_en.Id, "keywords");
                    if (keywords != null)
                    {
                        ViewBag.keywords = keywords.Value;
                    }
                    if (metadesc != null)
                    {
                        ViewBag.metadesc = metadesc.Value;
                    }
                    ViewBag.lang = "en";
                    ViewData["cate_img"] = post_model_en.Category.Img;
                    ViewData["cate_imgmobile"] = post_model_en.Category.ImgMobile;
                    ViewData["img_link"] = get_cate_meta(post_model_en.Category.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(post_model_en.Category.Id, "img_link_en");
                    if (post_model_en.CategoryId == 94)
                    {
                        if (post_model_en.View == null)
                        {
                            post_model_en.View = 0;

                        }
                        post_model_en.View += 1;

                        dc.SaveChanges();
                        return View("HocBong", post_model_en);
                    }
                    if (post_model_en.CategoryId == 95)
                    {
                        if (post_model_en.View == null)
                        {
                            post_model_en.View = 0;

                        }
                        post_model_en.View += 1;

                        dc.SaveChanges();
                        return View("HocBongIDP", post_model_en);
                    }
                    if (post_model_en.Category.ParentId == 48)
                    {
                        if (post_model_en.View == null)
                        {
                            post_model_en.View = 0;

                        }
                        post_model_en.View += 1;

                        dc.SaveChanges();
                        var post_related = dc.Posts.Include("Category").Where(p => p.CategoryId == post_model_en.CategoryId && p.Status == 1 && p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).Where(p => p.Id != post_model_en.Id && p.Status == 1).OrderByDescending(p => p.View).Take(8).ToList();
                        ViewData["post_related"] = post_related;
                        ViewData["cate_img"] = post_model_en.Category.Category1.Img;
                        ViewData["cate_imgmobile"] = post_model_en.Category.Category1.ImgMobile;
                        ViewData["img_link"] = get_cate_meta(post_model_en.Category.Category1.Id, "img_link") != null ? get_cate_meta(post_model_en.Category.Category1.Id, "img_link") : post_model_en.Category.Category1.Img;
                        ViewData["img_link_en"] = get_cate_meta(post_model_en.Category.Category1.Id, "img_link_en") != null ? get_cate_meta(post_model_en.Category.Category1.Id, "img_link_en") : post_model_en.Category.Category1.Img;
                        return View("TestimonialDetail", post_model_en);
                    }
                    if (post_model_en.CategoryId == 103)
                    {

                        return View("TipsDetail", post_model_en);
                    }
                    if (post_model_en.Category.ParentId == 47)
                    {
                        if (post_model_en.View == null)
                        {
                            post_model_en.View = 0;

                        }
                        post_model_en.View += 1;
                        dc.SaveChanges();
                        var post_related = dc.Posts.Include("Category").Include("Location").Where(p => p.CategoryId == post_model_en.CategoryId && p.Id != post_model_en.Id && p.Status == 1 && p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).OrderByDescending(p => p.Created).Take(3).ToList();
                        var post_lasted = dc.Posts.Include("Category").Include("Location").Where(p => (p.Category.ParentId == 47) && p.Status == 1 && p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).OrderByDescending(p => p.Created).Take(5).ToList();
                        var post_hotest = dc.Posts.Include("Category").Include("Location").Where(p => (p.Category.ParentId == 47) && p.Status == 1 && p.TitleEng != null && p.ContentEng != null && !p.TitleEng.Equals("")).Where(p => p.Created > datePast30).OrderByDescending(p => p.View).Take(5).ToList();
                        ViewData["post_hotest"] = post_hotest;
                        ViewData["post_related"] = post_related;
                        ViewData["post_lasted"] = post_lasted;

                        ViewData["cate_img"] = get_cate_meta(post_model_en.Id, "banner");
                        ViewData["cate_imgmobile"] = get_cate_meta(post_model_en.Id, "banner_m");
                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";

                        return View("NewsDetail", post_model_en);
                    }
                    if (post_model_en.CategoryId == 99)
                    {
                        if (post_model_en.View == null)
                        {
                            post_model_en.View = 0;

                        }
                        post_model_en.View += 1;
                        dc.SaveChanges();
                        ViewData["cate_img"] = post_model_en.Image;
                        if (post_model_en.Image != null)
                        {
                            var t_ext = Path.GetExtension(post_model_en.Image.ToString());
                            int t_length = post_model_en.Image.ToString().Length;
                            var t_name = post_model_en.Image.Substring(0, t_length - t_ext.Length);
                            ViewData["cate_imgmobile"] = t_name + "_mobile" + t_ext;
                        }
                        else
                        {
                            ViewData["cate_imgmobile"] = null;
                        }

                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";
                        return View("PageDetail", post_model_en);
                    }
                    if (post_model_en.CategoryId == 104)
                    {
                        if (post_model_en.View == null)
                        {
                            post_model_en.View = 0;

                        }
                        post_model_en.View += 1;
                        dc.SaveChanges();
                        ViewData["cate_img"] = post_model_en.Image;
                        if (post_model_en.Image != null)
                        {
                            var t_ext = Path.GetExtension(post_model_en.Image.ToString());
                            int t_length = post_model_en.Image.ToString().Length;
                            var t_name = post_model_en.Image.Substring(0, t_length - t_ext.Length);
                            ViewData["cate_imgmobile"] = t_name + "_mobile" + t_ext;
                        }
                        else
                        {
                            ViewData["cate_imgmobile"] = null;
                        }

                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";
                        return View("PageFull", post_model_en);
                    }
                    ViewData["posts"] = posts_en.OrderBy(c => c.order).ToList();
                    if (post_model_en.CategoryId == 45)
                    {
                        return View("PostAbout", post_model_en);
                    }
                    switch (post_model_en.CategoryId)
                    {
                        case 96:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("FS Box English")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                        case 97:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("AE Box English")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                        case 98:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("IELTS Box English")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                    }
                    return View(post_model_en);
                }
            }
        }


        public ActionResult Recruitment(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    if (this.Request.Path.Equals("/tuyen-dung/nhan-vien.html"))
                    {
                        return Redirect("/recruitment/non-academic.html");
                    }
                    if (this.Request.Path.Equals("/tuyen-dung/giang-vien.html"))
                    {
                        return Redirect("/recruitment/academic.html");
                    }
                    return Redirect("/recruitment.html");
                }
                else
                {
                    if (this.Request.Path.Equals("/recruitment/non-academic.html"))
                    {
                        return Redirect("/tuyen-dung/nhan-vien.html");
                    }
                    if (this.Request.Path.Equals("/recruitment/academic.html"))
                    {
                        return Redirect("/tuyen-dung/giang-vien.html");
                    }
                    return Redirect("/tuyen-dung.html");
                }
            }
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var post_1 = dc.Posts.Where(p => p.Category.ParentId == 49 && p.Status == 1).OrderByDescending(p => p.Created).ToList();
                return View(post_1);
            }
        }

        public ActionResult RecruitmentDetail(string id, string lang = "")
        {

            using (acet_dbEntities dc = new acet_dbEntities())
            {
                if (lang.Equals("en"))
                {
                    var post = dc.Posts.FirstOrDefault(p => p.SlugEng.Equals(id) && p.Category.ParentId == 49 && p.Status == 1);
                    if (post != null)
                    {
                        if (this.Request.QueryString["hl"] != null)
                        {
                            if (this.Request.QueryString["hl"].Equals("vi"))
                            {
                                return Redirect("/tuyen-dung/" + post.Category.Slug + "/" + post.Slug + ".html");
                            }
                        }
                    }
                    if (post.View == null)
                    {
                        post.View = 0;

                    }
                    post.View += 1;

                    dc.SaveChanges();
                    return View(post);
                }
                else
                {
                    var post = dc.Posts.FirstOrDefault(p => p.Slug.Equals(id) && p.Category.ParentId == 49 && p.Status == 1);
                    if (post != null)
                    {
                        if (this.Request.QueryString["hl"] != null)
                        {
                            if (this.Request.QueryString["hl"].Equals("en"))
                            {
                                return Redirect("/recruitment/" + post.Category.SlugEng + "/" + post.SlugEng + ".html");
                            }
                        }
                    }
                    if (post.View == null)
                    {
                        post.View = 0;

                    }
                    post.View += 1;

                    dc.SaveChanges();
                    return View(post);
                }

            }
        }

        public ActionResult HocBong(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                if (this.Request.QueryString["hl"] != null)
                {
                    if (this.Request.QueryString["hl"].Equals("en"))
                    {
                        return Redirect("/scholarship.html");
                    }
                    else
                    {
                        return Redirect("/hoc-bong.html");
                    }
                }
                var post = dc.Posts.Where(p => p.CategoryId == 94 && p.Status == 1).OrderByDescending(p => p.Created).FirstOrDefault();
                if (post.View == null)
                {
                    post.View = 0;

                }
                post.View += 1;

                dc.SaveChanges();
                ViewData["cate_img"] = post.Category.Img;
                ViewData["cate_imgmobile"] = post.Category.ImgMobile;
                ViewData["img_link"] = get_cate_meta(post.Category.Id, "img_link");
                ViewData["img_link_en"] = get_cate_meta(post.Category.Id, "img_link_en");
                return View(post);
            }
        }

        public ActionResult HocBongIDP(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    return Redirect("/idp-scholarship.html");
                }
                else
                {
                    return Redirect("/hoc-bong-idp.html");
                }
            }
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                var post = dc.Posts.Where(p => p.CategoryId == 95 && p.Status == 1).OrderByDescending(p => p.Created).FirstOrDefault();
                if (post.View == null)
                {
                    post.View = 0;

                }
                post.View += 1;

                dc.SaveChanges();
                ViewData["cate_img"] = post.Category.Img;
                ViewData["cate_imgmobile"] = post.Category.ImgMobile;
                ViewData["img_link"] = get_cate_meta(post.Category.Id, "img_link");
                ViewData["img_link_en"] = get_cate_meta(post.Category.Id, "img_link_en");
                return View(post);
            }
        }

        public ActionResult About(string lang = "")
        {
            return View();
        }

        public ActionResult Contact(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {

                if (this.Request.QueryString["hl"] != null)
                {
                    if (this.Request.QueryString["hl"].Equals("en"))
                    {
                        return Redirect("/contact-us.html");
                    }
                    else
                    {
                        return Redirect("/lien-he.html");
                    }
                }
                else
                {
                    var post = dc.Modules.Where(p => p.Position.Equals("HN"));
                    var post_cm = dc.Modules.Where(p => p.Position.Equals("HCM"));
                    if (getLang().Equals("vi"))
                    {
                        post = post.Where(p => p.Order == 1);
                        post_cm = post_cm.Where(p => p.Order == 1);
                    }
                    else
                    {
                        post = post.Where(p => p.Order == 0);
                        post_cm = post_cm.Where(p => p.Order == 0);
                    }
                    ViewBag.lhn = post.ToList();
                    ViewBag.lhcm = post_cm.ToList();
                    return View();
                }
            }
        }

        public ActionResult TKB(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                if (this.Request.QueryString["hl"] != null)
                {
                    if (this.Request.QueryString["hl"].Equals("en"))
                    {
                        return Redirect("/timetable.html");
                    }
                    else
                    {
                        return Redirect("/thoi-khoa-bieu.html");
                    }
                }
                ViewBag.post_ae = dc.Posts.FirstOrDefault(p => p.Id == 1738);
                ViewBag.post_ielts = dc.Posts.FirstOrDefault(p => p.Id == 1742);
                ViewBag.post_fs = dc.Posts.FirstOrDefault(p => p.Id == 1755);
                ViewBag.TKB_script = dc.Modules.Where(m => m.Position.Equals("TKB Script")).OrderBy(m => m.Order).FirstOrDefault();
                return View();
            }

        }

        public ActionResult Error404(string lang = "")
        {
            return View();
        }

        public ActionResult RegisterAll(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    return Redirect("/course-register.html");
                }
                else
                {
                    return Redirect("/dang-ky-khoa-hoc.html");
                }
            }
            return View();
        }

        public ActionResult UnderConstruction(string lang = "")
        {
            return View();
        }

        public ActionResult Thankyou(string lang = "")
        {
            return View();
        }

        public ActionResult Download(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    return Redirect("/free-ielts-documents.html");
                }
                else
                {
                    return Redirect("/tai-tai-lieu-luyen-thi-ielts-mien-phi.html");
                }
            }
            return View();
        }

        public ActionResult DownloadDetail(string lang = "")
        {
            if (this.Request.QueryString["hl"] != null)
            {
                if (this.Request.QueryString["hl"].Equals("en"))
                {
                    return Redirect("/download/free-ielts-documents.html");
                }
                else
                {
                    return Redirect("/download/tai-tai-lieu-luyen-thi-ielts-mien-phi.html");
                }
            }
            return View();
        }
        public ActionResult Sitemap2(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                Response.Clear();
                Response.ContentType = "text/xml";
                SiteMapFeedGenerator gen = new SiteMapFeedGenerator(Response.Output);
                var posts = dc.Posts.Include("Category").Where(p => p.Status == 1 && p.Content != null && p.Slug != null && !p.Slug.Equals("") && p.Category.Slug != null && !p.Category.Name.Equals("") && p.Title != null && !p.Title.Equals("")).OrderByDescending(p => p.Modified);
                gen.WriteStartDocument();
                foreach (var item in posts.ToList())
                {
                    var cate_slug = item.Category.Slug;
                    if (item.Category.ParentId == 48)
                    {
                        cate_slug = item.Category.Category1.Slug;
                    }
                    if (item.Category.ParentId == 49)
                    {
                        cate_slug = item.Category.Category1.Slug + "/" + item.Category.Slug;
                    }
                    gen.WriteItem(this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/" + cate_slug + "/" + item.Slug + ".html", (DateTime)item.Modified, "0.8", "always");
                }
                var posts2 = dc.Categories.Where(p => p.Status == 1 && p.Slug != null && p.ParentId == 47);
                var base_host = this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/";
                foreach (var item2 in posts2.ToList())
                {

                    gen.WriteItem(base_host + item2.Slug + ".html", DateTime.Now, "0.8", "always");
                }
                gen.WriteItem(base_host + "luyen-thi-ielts.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "anh-ngu-hoc-thuat.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "tieng-anh-thcs-first-steps.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "kiem-tra-tieng-anh-truc-tuyen.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "tai-tai-lieu-luyen-thi-ielts-mien-phi.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "dang-ky-khoa-hoc.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "thoi-khoa-bieu.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "tuyen-dung.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "hoc-bong.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "hoc-bong-idp.html", DateTime.Now, "0.8", "always");
                gen.WriteItem(base_host + "lien-he.html", DateTime.Now, "0.8", "always");
                gen.WriteEndDocument();
                gen.Close();
                return View();
            }

        }
        public ActionResult SitemapIndex(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                Response.Clear();
                Response.ContentType = "text/xml";
                SiteMapFeedGenerator gen = new SiteMapFeedGenerator(Response.Output);
                var postCount = dc.Posts.Where(p => p.Status == 1 && p.Content != null && p.Title != null && !p.Title.Equals("")).Count();
                gen.WriteStartDocumentIndex();
                var totalPage = (int)postCount / 1000;
                for (var i = 0; i <= totalPage; i++)
                {
                    gen.WriteItem(this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/sitemap-post-" + i + ".xml", DateTime.Now, "1.0", "weekly");
                }
                gen.WriteItem(this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/sitemap-category.xml", DateTime.Now, "1.0", "weekly");

                gen.WriteEndDocument();
                gen.Close();
                return View();
            }
        }
        public ActionResult Sitemap(int page = 0, string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                Response.Clear();
                Response.ContentType = "text/xml";
                SiteMapFeedGenerator gen = new SiteMapFeedGenerator(Response.Output);
                var posts = dc.Posts.Include("Category").Where(p => p.Status == 1 && p.Content != null && p.Title != null && !p.Title.Equals("")).OrderByDescending(p => p.Id).Skip(1000 * page).Take(1000);
                gen.WriteStartDocument();
                foreach (var item in posts.ToList())
                {
                    var cate_slug = item.Category.Slug;
                    if (item.Category.ParentId == 48)
                    {
                        cate_slug = item.Category.Category1.Slug;
                    }
                    if (item.Category.ParentId == 49)
                    {
                        cate_slug = item.Category.Category1.Slug + "/" + item.Category.Slug;
                    }
                    gen.WriteItem(this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/" + cate_slug + "/" + item.Slug + ".html", (DateTime)item.Modified, "0.6", "daily");
                }

                gen.WriteEndDocument();
                gen.Close();
                return View();
            }
        }
        public ActionResult SitemapCategory(string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                Response.Clear();
                Response.ContentType = "text/xml";
                SiteMapFeedGenerator gen = new SiteMapFeedGenerator(Response.Output);
                var posts = dc.Categories.Where(p => p.Status == 1 && p.Slug != null && p.ParentId == 47);
                gen.WriteStartDocument();
                var base_host = this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/";
                foreach (var item in posts.ToList())
                {

                    gen.WriteItem(base_host + item.Slug + ".html", DateTime.Now, "0.6", "weekly");
                }
                gen.WriteItem(base_host + "luyen-thi-ielts.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "anh-ngu-hoc-thuat.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "tieng-anh-thcs-first-steps.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "kiem-tra-tieng-anh-truc-tuyen.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "tai-tai-lieu-luyen-thi-ielts-mien-phi.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "dang-ky-khoa-hoc.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "thoi-khoa-bieu.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "tuyen-dung.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "hoc-bong.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "hoc-bong-idp.html", DateTime.Now, "0.6", "weekly");
                gen.WriteItem(base_host + "lien-he.html", DateTime.Now, "0.6", "weekly");
                gen.WriteEndDocument();
                gen.Close();
                return View("Sitemap");
            }
        }
        public ActionResult RSS(string slug = "", string lang = "")
        {
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                Response.Clear();
                Response.ContentType = "text/xml";
                SiteMapFeedGenerator gen = new SiteMapFeedGenerator(Response.Output);
                var cate = dc.Categories.Where(p => p.Slug.Equals(slug)).FirstOrDefault();
                var site_title = "ACET";
                var link = this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/";
                var site_desc = "ACET";

                gen.WriteStartDocumentRSS(site_title, link, site_desc, this.Request.Url.Host);
                if (cate != null)
                {
                    if (cate.Id == 48)
                    {
                        var posts = dc.Posts.Where(p => p.Category.ParentId == cate.Id && p.Status == 1 && p.Content != null && p.Title != null && !p.Title.Equals("")).OrderByDescending(p => p.Id).Skip(0).Take(20);
                        foreach (var item in posts.ToList())
                        {
                            var cate_slug = item.Category.Category1.Slug;

                            gen.WriteItemRSS(this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/" + cate_slug + "/" + item.Slug + ".html", (DateTime)item.Modified, item.Title, item.Desc);

                        }
                    }
                    if (cate.ParentId == 47)
                    {
                        var posts = dc.Posts.Where(p => p.CategoryId == cate.Id && p.Status == 1 && p.Content != null && p.Title != null && !p.Title.Equals("")).OrderByDescending(p => p.Id).Skip(0).Take(20);
                        foreach (var item in posts.ToList())
                        {
                            var cate_slug = item.Category.Slug;

                            gen.WriteItemRSS(this.Request.Url.Scheme + "://" + this.Request.Url.Host + "/" + cate_slug + "/" + item.Slug + ".html", (DateTime)item.Modified, item.Title, item.Desc);

                        }
                    }


                }


                gen.WriteEndDocumentRSS();
                gen.Close();
                return View("Sitemap");
            }
        }

        public ActionResult Review(string lang = "")
        {
            //			try {
            var id = Int32.Parse(this.Request.QueryString["id"]);
            using (acet_dbEntities dc = new acet_dbEntities())
            {
                if (this.Request.QueryString["hl"] != null)
                    lang = this.Request.QueryString["hl"];

                var post_model = dc.Posts.Include("Category").Include("Location").FirstOrDefault(p => p.Id.Equals(id) && p.Status == -1);
                if (lang.Equals("en"))
                {
                    ViewBag.lang = "en";
                }
                else
                {
                    ViewBag.lang = "vi";
                }
                DateTime dateTimeNow = DateTime.UtcNow;
                DateTime datePast30 = dateTimeNow.Date.AddDays(-60);
                if (post_model != null)
                {
                    var metadesc = getMeta(post_model.Id, "metadesc");
                    var keywords = getMeta(post_model.Id, "keywords");
                    if (keywords != null)
                    {
                        ViewBag.keywords = keywords.Value;
                    }
                    if (metadesc != null)
                    {
                        ViewBag.metadesc = metadesc.Value;
                    }
                    ViewData["cate_img"] = post_model.Category.Img;
                    ViewData["cate_imgmobile"] = post_model.Category.ImgMobile;

                    ViewData["img_link"] = get_cate_meta(post_model.Category.Id, "img_link");
                    ViewData["img_link_en"] = get_cate_meta(post_model.Category.Id, "img_link_en");
                    if (post_model.CategoryId == 94)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;

                        dc.SaveChanges();
                        return View("HocBong", post_model);
                    }
                    if (post_model.CategoryId == 95)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;

                        dc.SaveChanges();
                        return View("HocBongIDP", post_model);
                    }
                    if (post_model.Category.ParentId == 48)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;

                        dc.SaveChanges();
                        var post_related = dc.Posts.Include("Category").Where(p => p.CategoryId == post_model.CategoryId && p.Status == 1).Where(p => p.Id != post_model.Id && p.Status == 1).OrderByDescending(p => p.View).Take(8).ToList();
                        ViewData["post_related"] = post_related;
                        ViewData["cate_img"] = post_model.Category.Category1.Img;
                        ViewData["cate_imgmobile"] = post_model.Category.Category1.ImgMobile;
                        ViewData["img_link"] = get_cate_meta(post_model.Category.Category1.Id, "img_link");
                        ViewData["img_link_en"] = get_cate_meta(post_model.Category.Category1.Id, "img_link_en");

                        return View("TestimonialDetail", post_model);
                    }
                    if (post_model.CategoryId == 103)
                    {

                        return View("TipsDetail", post_model);
                    }
                    if (post_model.Category.ParentId == 47)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;
                        dc.SaveChanges();
                        var post_related = dc.Posts.Include("Category").Include("Location").Where(p => p.CategoryId == post_model.CategoryId && p.Id != post_model.Id && p.Status == 1).OrderByDescending(p => p.Created).Take(3).ToList();
                        var post_lasted = dc.Posts.Include("Category").Include("Location").Where(p => (p.Category.ParentId == 47) && p.Status == 1).OrderByDescending(p => p.Created).Take(5).ToList();
                        var post_hotest = dc.Posts.Include("Category").Include("Location").Where(p => (p.Category.ParentId == 47) && p.Status == 1).Where(p => p.Created > datePast30).OrderByDescending(p => p.View).Take(5).ToList();
                        ViewData["post_hotest"] = post_hotest;
                        ViewData["post_related"] = post_related;
                        ViewData["post_lasted"] = post_lasted;

                        ViewData["cate_img"] = get_cate_meta(post_model.Id, "banner");
                        ViewData["cate_imgmobile"] = get_cate_meta(post_model.Id, "banner_m");
                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";

                        return View("NewsDetail", post_model);
                    }
                    if (post_model.CategoryId == 99)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;
                        dc.SaveChanges();
                        ViewData["cate_img"] = post_model.Image;
                        if (post_model.Image != null)
                        {
                            var t_ext = Path.GetExtension(post_model.Image.ToString());
                            int t_length = post_model.Image.ToString().Length;
                            var t_name = post_model.Image.Substring(0, t_length - t_ext.Length);
                            ViewData["cate_imgmobile"] = t_name + "_mobile" + t_ext;
                        }
                        else
                        {
                            ViewData["cate_imgmobile"] = null;
                        }

                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";
                        return View("PageDetail", post_model);
                    }
                    if (post_model.CategoryId == 104)
                    {
                        if (post_model.View == null)
                        {
                            post_model.View = 0;

                        }
                        post_model.View += 1;
                        dc.SaveChanges();
                        ViewData["cate_img"] = post_model.Image;
                        if (post_model.Image != null)
                        {
                            var t_ext = Path.GetExtension(post_model.Image.ToString());
                            int t_length = post_model.Image.ToString().Length;
                            var t_name = post_model.Image.Substring(0, t_length - t_ext.Length);
                            ViewData["cate_imgmobile"] = t_name + "_mobile" + t_ext;
                        }
                        else
                        {
                            ViewData["cate_imgmobile"] = null;
                        }

                        ViewData["img_link"] = "#";
                        ViewData["img_link_en"] = "#";
                        return View("PageFull", post_model);
                    }
                    var posts = dc.Posts.Include("Category").Include("Location").Where(p => p.Category.Id == post_model.CategoryId || p.Category.Category1.Id == post_model.CategoryId).Where(p => p.Status == 1).OrderBy(p => p.order).ToList();

                    ViewData["posts"] = posts;
                    if (post_model.CategoryId == 45)
                    {
                        return View("PostAbout", post_model);
                    }
                    switch (post_model.CategoryId)
                    {
                        case 96:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("FS Box")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                        case 97:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("AE Box")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                        case 98:
                            ViewBag.OtherBox = dc.Modules.Where(m => m.Position.Equals("IELTS Box")).OrderBy(m => m.Order).FirstOrDefault();
                            break;
                    }
                    return View(post_model);

                }
                else
                {
                    return Redirect("/");
                }
            }

            //			} catch (Exception e) {
            //				return Redirect ("/");
            //			}
        }

        public ActionResult CourseDetail_old(string lang = "")
        {
            return RedirectPermanent("/tieng-anh-thcs-first-steps.html");
        }
    }
}
