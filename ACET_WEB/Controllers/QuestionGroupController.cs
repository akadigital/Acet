﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Controllers
{
	[Authorize(Roles = "Administrator")]
    public class QuestionGroupController : Controller
    {
        private acet_dbEntities db = new acet_dbEntities();

        //
        // GET: /QuestionGroup/

        public ActionResult Index()
        {
            return View(db.QuestionGroups.ToList());
        }

        //
        // GET: /QuestionGroup/Details/5

        public ActionResult Details(int id = 0)
        {
            QuestionGroup questiongroup = db.QuestionGroups.Single(q => q.Id == id);
            if (questiongroup == null)
            {
                return HttpNotFound();
            }
            return View(questiongroup);
        }

        //
        // GET: /QuestionGroup/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /QuestionGroup/Create

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionGroup questiongroup)
        {
            if (ModelState.IsValid)
            {
                db.QuestionGroups.AddObject(questiongroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(questiongroup);
        }

        //
        // GET: /QuestionGroup/Edit/5
        public ActionResult Edit(int id = 0)
        {
            QuestionGroup questiongroup = db.QuestionGroups.Single(q => q.Id == id);
            if (questiongroup == null)
            {
                return HttpNotFound();
            }
            return View(questiongroup);
        }

        //
        // POST: /QuestionGroup/Edit/5

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionGroup questiongroup)
        {
            if (ModelState.IsValid)
            {
                db.QuestionGroups.Attach(questiongroup);
                db.ObjectStateManager.ChangeObjectState(questiongroup, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(questiongroup);
        }

        //
        // GET: /QuestionGroup/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuestionGroup questiongroup = db.QuestionGroups.Single(q => q.Id == id);
            if (questiongroup == null)
            {
                return HttpNotFound();
            }
            return View(questiongroup);
        }

        //
        // POST: /QuestionGroup/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionGroup questiongroup = db.QuestionGroups.Single(q => q.Id == id);
            db.QuestionGroups.DeleteObject(questiongroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}