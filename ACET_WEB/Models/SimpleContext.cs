﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ACET_WEB
{
	public class SimpleContext: DbContext
	{
		public SimpleContext (): base("name=SimpleContext")
		{
		}
//		protected override void OnModelCreating(DbModelBuilder modelBuilder)
//		{
//			throw new UnintentionalCodeFirstException();
//		}
		public virtual DbSet<PostMeta> PostMetas {  get;  set; }
	}
}

