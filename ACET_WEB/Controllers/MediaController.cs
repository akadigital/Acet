﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACET_WEB.Controllers
{
	[Authorize(Roles = "Administrator")]
    public class MediaController : Controller
    {
        private acet_dbEntities db = new acet_dbEntities();

        //
        // GET: /Media/

        public ActionResult Index()
        {
            var media = db.Media.Include("MediaCategory").Where(p => p.Status == 1);
            return View(media.ToList());
        }

        //
        // GET: /Media/Details/5

        public ActionResult Details(int id = 0)
        {
            Medias medias = db.Media.Single(m => m.Id == id);
            if (medias == null)
            {
                return HttpNotFound();
            }
            return View(medias);
        }

        //
        // GET: /Media/Create

        public ActionResult Create()
        {
            
            ViewBag.CategoryId = new SelectList(db.MediaCategories.Where(p => p.Status == 1), "Id", "Name");
            
            return View();
        }

        //
        // POST: /Media/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Medias medias)
        {
            if (ModelState.IsValid)
            {
                medias.Status = 1;
                //medias.Created = DateTime.Now;
                medias.Modified = DateTime.Now;
                db.Media.AddObject(medias);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.MediaCategories, "Id", "Name", medias.CategoryId);
            return View(medias);
        }

        //
        // GET: /Media/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Medias medias = db.Media.Single(m => m.Id == id);
            if (medias == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.MediaCategories, "Id", "Name", medias.CategoryId);
            return View(medias);
        }

        //
        // POST: /Media/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Medias medias)
        {
            if (ModelState.IsValid)
            {
                medias.Modified = DateTime.Now;
                db.Media.Attach(medias);
                db.ObjectStateManager.ChangeObjectState(medias, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.MediaCategories, "Id", "Name", medias.CategoryId);
            return View(medias);
        }

        //
        // GET: /Media/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Medias medias = db.Media.Single(m => m.Id == id);
            if (medias == null)
            {
                return HttpNotFound();
            }
            return View(medias);
        }

        //
        // POST: /Media/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Medias medias = db.Media.Single(m => m.Id == id);
            db.Media.DeleteObject(medias);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}