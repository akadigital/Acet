// FullCalendar v1.5
var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();

var color_maps = {
	"First Steps":'#ff4400',
	"Academic English":"blue",
	"IELTS":"green"
};
var hour_maps = [{
	"First Steps":' 01:00',
	"Academic English":" 03:00",
	"IELTS":" 06:00"
},{
	"First Steps":' 02:00',
	"Academic English":" 04:00",
	"IELTS":" 07:00"
}
]
function build_event_obj(obj){
	return { title: 'Khai giảng ' + obj[2],
			 start: new Date(obj[1]+hour_maps[0][obj[2]]),
			 end: new Date(obj[1]+hour_maps[1][obj[2]]),
			 allDay: false,
			 color: color_maps[obj[2]]
			 };
}
var current_event = [
{
    title: 'Khai giảng First Steps',
    start: new Date(2015, 6-1, 1, 0, 0),
    end: new Date(2015, 6-1, 1, 1, 0),
    allDay: false,
    color: '#ff4400'
},
 {
    title: 'Khai giảng First Steps',
    start: new Date(2015, 6-1, 6, 0, 0),
    end: new Date(2015, 6-1, 6, 1, 0),
    allDay: false,
    color: '#ff4400'
},
 {
    title: 'Khai giảng First Steps',
    start: new Date(2015, 8-1, 22, 0, 0),
    end: new Date(2015, 8-1, 22, 1, 0),
    allDay: false,
    color: '#ff4400'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 6-1, 1, 2, 0),
    end: new Date(2015, 6-1, 1, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 7-1, 6, 2, 0),
    end: new Date(2015, 7-1, 6, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 8-1, 10, 2, 0),
    end: new Date(2015, 8-1, 10, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 12-1, 28, 2, 0),
    end: new Date(2015, 12-1, 28, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 11-1, 23, 2, 0),
    end: new Date(2015, 11-1, 23, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 10-1, 19, 2, 0),
    end: new Date(2015, 10-1, 19, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng Academic',
      start: new Date(2015, 9-1, 14, 2, 0),
    end: new Date(2015, 9-1, 14, 3, 0),
    allDay: false,
    color: 'blue'
},
{
    title: 'Khai giảng IELTS', 
      start: new Date(2015, 9-1, 14, 2, 0),
    end: new Date(2015, 9-1, 14, 3, 0),
    allDay: false,
    color: 'green'
},
{
    title: 'Khai giảng IELTS', 
      start: new Date(2015, 10-1, 19, 2, 0),
    end: new Date(2015, 10-1, 19, 3, 0),
    allDay: false,
    color: 'green'
},
{
    title: 'Khai giảng IELTS', 
      start: new Date(2015, 11-1, 23, 2, 0),
    end: new Date(2015, 11-1, 23, 3, 0),
    allDay: false,
    color: 'green'
},
{
    title: 'Khai giảng IELTS', 
      start: new Date(2015, 12-1, 28, 2, 0),
    end: new Date(2015, 12-1, 28, 3, 0),
    allDay: false,
    color: 'green'
},
{
    title: 'Khai giảng IELTS', 
    start: new Date(2015, 6-1, 1, 4, 0),
    end: new Date(2015, 6-1, 1, 5, 0),
    allDay: false,
    color: 'green'
},
{
    title: 'Khai giảng IELTS', 
       start: new Date(2015, 7-1, 6, 4, 0),
    end: new Date(2015, 7-1, 6, 5, 0),
    allDay: false,
    color: 'green'
},
{
    title: 'Khai giảng IELTS', 
    start: new Date(2015, 8-1, 10, 4, 0),
    end: new Date(2015, 8-1, 10, 5, 0),
    allDay: false,
    color: 'green'
},

];
$(function(){

    $.get('/api/calendars?draw=1&length=100&start=0&order_dir=asc&order_colunm=&search_value=').done(function(rs){
    	current_event = [];
    	$.each(rs.data, function(i,j){
    		current_event.push(build_event_obj(j));
    	});
    	 $('#calendar').fullCalendar({
	        theme: true,
	        dayNamesShort:['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	        header: {
	            left: 'prev',
	            center: ' title ',
	            right: 'next'
	        },
	        editable: false,
	        
	        // add event name to title attribute on mouseover
	        eventMouseover: function(event, jsEvent, view) {
	            if (view.name !== 'agendaDay') {
	                $(jsEvent.target).attr('title', event.title);
	            }
	        },
	        
	        // For DEMO only
	        // *************
	        events: current_event,
	        eventAfterAllRender: function(view){
	          re_position();
	          init_name_list(unique_event_name(current_event));
	        }
	        
	    });
    });

});
function unique_event_name(events){
  var rs =[];
  var rs_2 = [];
  $(events).each(function(i,j){
    if(rs_2.indexOf(j.title)<0){
      rs.push(j);
      rs_2.push(j.title);
    }
  });
  return rs;
}

function init_name_list(unique_event_name){
 var  htmlstr = '';
  $(unique_event_name).each(function(i,j){
    htmlstr += '<li><i class="fa fa-circle smaller-9 " style="color:'+j.color+'"></i>'+j.title+'</li>'
  });
  $('#list_calendar').html(htmlstr);
}
/* ===== Tooltips ===== */
function re_position(){
  var l1 =[], l2=[], l3=[], l4=[], l5=[], l6=[], l7 = [];
  $('.fc-event-container .fc-event').each(function(i,j){
    var n1 = parseInt(j.style.top);
    var n2 = parseInt(j.style.left);
    if(n2==30){
      l2.push(j);
    }
    if(n2==2){
      l1.push(j);
    }
    if(n2==58){
      l3.push(j);
    }
     if(n2==86){
      l4.push(j);
    }
     if(n2==114){
      l5.push(j);
    }
     if(n2==142){
      l6.push(j);
    }
     if(n2==170){
      l7.push(j);
    }
    if(52> n1){
      $(this).addClass("top30");
    }
    if(52<= n1&& n1<74){
      $(this).addClass("top52");
    }
    if(74<= n1 && n1 <96){
      $(this).addClass("top74");
    }
    if(96<= n1 && n1 <118){
      $(this).addClass("top96");
    }
    if(118<= n1 && n1 <140){
      $(this).addClass("top118");
    }
    if(140<= n1 && n1 <162){
      $(this).addClass("top140");
    }
  });
  $(l1).each(function(o1,m1){
    $(m1).addClass("left2_" + o1);
  });
  $(l2).each(function(o2,m2){
    $(m2).addClass("left30_" + o2);
  });
  $(l3).each(function(o2,m2){
    $(m2).addClass("left58_" + o2);
  });
  $(l4).each(function(o2,m2){
    $(m2).addClass("left86_" + o2);
  });
  $(l5).each(function(o2,m2){
    $(m2).addClass("left114_" + o2);
  });
  $(l6).each(function(o2,m2){
    $(m2).addClass("left142_" + o2);
  });
  $(l7).each(function(o2,m2){
    $(m2).addClass("left170_" + o2);
  });
}

$('#tooltip').tooltip();
$(window).on("scroll touchmove", function () {
  $('.navbar-header').toggleClass('tiny', $(document).scrollTop() > 0);
  $('.wrapper').toggleClass('padding-top-80', $(document).scrollTop() > 0);
  $('.navbar').toggleClass('min-height-45', $(document).scrollTop() > 0);

  if($(document).scrollTop()>=1654){
  	var fix_top = 1654 - $(document).scrollTop() +55;
	$('#dvRight').attr("style","display:block;top: " + fix_top +"px");
  $('#dvLeft').attr("style","display:block;top: " + fix_top +"px");
   if($('.fc-view').children().length==0){
     $('#calendar').fullCalendar('render');
   }
  }else{
  //if($(document).scrollTop()>1){
  	//735
	//var fix_top = $("#six_symbol").position().top - $(document).scrollTop() - 20;
	var fix_top;
	if ($(".home-services").length > 0)
	    fix_top = $(".home-services").position().top - $(document).scrollTop() + 180;
	else
		   fix_top = 735 - $(document).scrollTop() + 55;
	if(fix_top<45){
		   fix_top=45;
	}

  	$('#dvRight').attr("style","display:block;top: " + fix_top +"px");
    $('#dvLeft').attr("style","display:block;top: " + fix_top +"px");
      if($('.fc-view').children().length==0){
     $('#calendar').fullCalendar('render');
   }
  //}else {
  		//$('#dvRight').attr("style","display:none");
     // $('#dvLeft').attr("style","display:none");
  //}
}
});
$('#countdown1').ClassyCountdown({
    theme: "white",
    end: $.now() + 645600,
    labels: true,
    labelsOptions: {
      lang: {
          days: '&nbsp;&nbsp;&nbsp;Days',
          hours: '&nbsp;&nbsp;Hours',
          minutes: 'Minutes',
          seconds: 'Seconds'
        },
        style: 'font-size: 0.4em;color:#fff;position: absolute; margin-top: -10px; margin-left: -15px;'
      },
    // custom style for the countdown
style: {
  element: '',
  labels: false,
  textResponsive: 0.5,
  days: {
    gauge: {
      thickness: 0.05,
      bgColor: 'rgba(218, 218, 218, 0.7)',
      fgColor: 'rgba(255, 255, 255, 1)',
      lineCap: 'butt'
    },
    textCSS: 'color:#fff; '
  },
  hours: {
    gauge: {
      thickness: 0.05,
      bgColor: 'rgba(218, 218, 218, 0.7)',
      fgColor: 'rgba(255, 255, 255, 1)',
      lineCap: 'butt'
    },
    textCSS: 'color:#fff; '
  },
  minutes: {
    gauge: {
      thickness: 0.05,
      bgColor: 'rgba(218, 218, 218, 0.7)',
      fgColor: 'rgba(255, 255, 255, 1)',
      lineCap: 'butt'
    },
    textCSS: 'color:#fff; '
  },
  seconds: {
    gauge: {
      thickness: 0.05,
      bgColor: 'rgba(218, 218, 218, 0.7)',
      fgColor: 'rgba(255, 255, 255, 1)',
      lineCap: 'butt'
    },
    textCSS: 'color:#fff; '
  }
},

// callback that is fired when the countdown reaches 0.
onEndCallback: function() {}

});
$(document).ready(function(){
  
 
});

$(document).ready(function(ev){
    $('#custom_carousel').on('slide.bs.carousel', function (evt) {
        $('#custom_carousel .controls li.active').removeClass('active');
        $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
    });
    var fix_top1;
    if ($(".home-services").length > 0)
        fix_top1 = $(".home-services").position().top - $(document).scrollTop() + 95;
    else
        fix_top1 = 735 - $(document).scrollTop() + 55;
    if (fix_top1 < 45) {
        fix_top1 = 45;
    }

    $('#dvRight').attr("style", "display:block;top: " + fix_top1 + "px");
    $('#dvLeft').attr("style", "display:block;top: " + fix_top1 + "px");
});

