﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ACET_WEB.App_Start;
namespace ACET_WEB.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        [HttpGet]
        public ActionResult Index()
        {
			if (this.Request.QueryString ["ReturnUrl"] != null) {
				ViewBag.ReturnUrl = this.Request.QueryString ["ReturnUrl"];
			}
            return View();
        }

        [HttpPost]
        public ActionResult Index(Models.LoginUsers loginUser)
        {
            if (ModelState.IsValid) {
                using (acet_dbEntities dc = new acet_dbEntities()) {
                    var passEncode = Helper.EncodeSHA1(loginUser.Password);
                    var v = dc.Users.Where(a => a.UserName.Equals(loginUser.UserName) && a.Password.Equals(passEncode) && a.Is_Active==1).FirstOrDefault();
                    if (v != null) {
                        Session["IsAuthorized"] = true;
                        Session["UserID"] = v.Id;
                        Session["Username"] = v.UserName;
                        FormsAuthentication.RedirectFromLoginPage(loginUser.UserName, loginUser.RememberMe);
						if (this.Request.QueryString ["ReturnUrl"] != null) {
							return Redirect (this.Request.QueryString ["ReturnUrl"]);
						}
                        return RedirectToAction("Index", "Admin");
                    }
                    else
                    {
                        ModelState.AddModelError("errorMsg", "Login data is incorrect!");
                    }
                }
            }
            return View(loginUser);
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}
